# PRAIOS
Code for PRAIOS, described in the paper: 
E. Egea-Lopez, P. Pavon-Mariño and J. Santa, "Optimal Joint Power And Rate Adaptation for Awareness and Congestion Control in Vehicular Networks." 
[10.1109/TITS.2022.3209094](https://dx.doi.org/10.1109/TITS.2022.3209094)

## Getting started
To run the CVX code just go to `python` folder and run `python -u opt.py`. You can configure 3 scenarios by commenting or uncommenting the configuration in the python file.

To run the OMNET scenarios, after installing everything, just run the CAMNetwork ned file and select the scenario from the `omnetpp.ini` file.


## Description
PRAIOS implements a distributed algorithm to achieve optimal joint adaptation of power and rate for congestion control in vehicular networks. Our approach is based on a network utility formulation of the congestion control problem, which allows us to induce a desired fairness
notion and set different priorities for vehicles. In this repository you can find the simulation code that we have used for its evaluation. 


The simulations have been implemented with the [OMNET++ 6.0 simulator](https://omnetpp.org/), and the [INET 4.3.7](https://inet.omnetpp.org/) framework. 
You can also find an implementation of the SAE J2945 standard and BFPC, which we compared to our proposal.

The paper will be available for download [here](https://dx.doi.org/10.1109/TITS.2022.3209094).

## Installation
To run the python code, install CVXPY following the instructions [here](https://www.cvxpy.org/install/index.html). 

To run the OMNET++ code, first install it, then install INET. Our code is to be run as part of INET, so you need to create the corresponding folders in the 
INET tree. For instance, the  `vehicular` folder with our code is to be created inside the `src/inet/applications/` folder. The full paths are already in the repository. 
In the `examples` folder you can find the scenario we have used to simulate. Just add this to the inet examples folder.


## Contributing


## License
MIT License

## Project status
On development
