import cvxpy as cp
import numpy as np
m=1 #Rayleigh fading
pkttime=752e-6 #message duration at some datarate
maxCapacity=1/pkttime #messages/s
S=np.power(10,-8.5)*1e-3 #Sensitivity  in W
c=299792458
f=5.9e9
w=c/f
A=np.power(4*np.pi/w,2)
pathloss=2.5


#Scenario 0. Validation
#x=np.arange(0,150,50)
#C=10
#hname='h3p.txt'
#yname='y3p.txt'

#Scenario 1. 38 vehicles
#x=np.arange(0,1900,50)
#cbrGoal=0.1
#C=cbrGoal*maxCapacity
##C=150
#hname='h38c01p.txt'
#yname='y38c01p.txt'


#Scenario 2. 286
x=np.arange(0,2860,10)
cbrGoal=0.4
C=cbrGoal*maxCapacity
hname='h286c04a4p.txt'
yname='y286c04a4p.txt'

cbr=C/maxCapacity
n=x.size
distances=np.zeros((n,n));
K=np.zeros((n,n));
for i in range(n):
    distances[i,:]=np.abs(x-x[i]);
    K[i,:]=S*A*m*np.power(distances[i,:],pathloss);
print(K)
#print(distances)
pmin=0.1 #100 mW in W
pmax=1 #1W
hmax=1/pmin #100 mW in W 
hmin=1/pmax #1000 mW in W
ymin=np.log(2.6)
ymax=np.log(10)
do=250
Ko=S*A*m*np.power(do,pathloss)
print('Configuration')
print('S',S)
print('A',A)
print('w',w)
print('pathloss',pathloss)
print('m',m)
print('C',C)
print('cbr',cbr)
print('maxCapacity',maxCapacity)
print('vehicles',x)
print('n',n)
print('hmax', hmax)
print('hmin', hmin)
print('ymax', ymax)
print('ymin', ymin)
print('Ko',Ko)

#Create CVXPY problem
alpha=cp.Parameter(nonneg=True)
alpha.value=2
h = cp.Variable((1,n))
y = cp.Variable((1,n))
print('h',h[0,1].shape)
print('y',y[0,1].shape)
print('alpha',alpha.value)
ob=0
cons=[]
cons=[ h >= hmin, h <= hmax, y >=ymin  , y <= ymax]
for i in range(n):
   ob += cp.exp((y[0,i]-Ko*h[0,i])*(1-alpha))
   cons += [cp.sum(cp.exp(y-cp.multiply(K[i,:].reshape(1,n),h)))<=C]

print((-Ko*h).shape)
prob=cp.Problem(cp.Minimize(ob),cons)

print('is DCP', prob.is_dcp())
prob.solve(verbose=True,  max_iters=10000)
#Solution
print("Status", prob.status)
print("Optimal var", prob.value)
print('h',h.value) # A numpy ndarray.
print('y',y.value) # A numpy ndarray.
print('rates', np.exp(y.value))
#Checks
Kh=K*h.value
#print(Kh)
cm=np.exp(y.value-Kh)
#print(cm)
cbropt=np.sum(cm,axis=1)/C
print('CBR result',cbropt )
print('Constraints not met at vehicle',np.nonzero(cbropt>1)) 
#Save results
np.savetxt(hname,h.value)
np.savetxt(yname,y.value)

