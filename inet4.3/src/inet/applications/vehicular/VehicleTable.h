//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET4_3_VEHICLETABLE_H_
#define __INET4_3_VEHICLETABLE_H_

#include <omnetpp.h>
#include "VehicleInfo.h"
#include "inet/mobility/contract/IMobility.h"

using namespace omnetpp;

namespace inet {


typedef std::map<int,VehicleInfo*> VTable;


class IRTHistogram : public cObject {
public:
    IRTHistogram(int cells, double size);
    std::vector<double> irts;
    std::vector<int> samples;
    double cellsize;
    double maxrange;
    void collect(double itime, double distance);
    double meanAtCell(int k);
    double meanAtDistance(double distance);
    double meanLessDistance(double distance);
    friend std::ostream& operator<<(std::ostream& out, const IRTHistogram& inf);
        std::string  toString() const;
        virtual std::string     info () const;
};
/**
 * TODO - Generated class
 */

class VehicleTable : public cSimpleModule
{
public:
    virtual ~VehicleTable();
    virtual int insertOrUpdate(VehicleInfo* info);
    virtual void refreshTable();
    virtual void refreshTable(double ut);
    virtual bool cancelUpdateTimer();
    VTable vt;
  protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    virtual void finish();

    IMobility* mob;

    cMessage* updateTimer;
    double updateTime;
    bool persistent;
    IRTHistogram* irthist;
    static simsignal_t neighbors;
    static simsignal_t irt;
    static simsignal_t rrt;
};


} //namespace

#endif
