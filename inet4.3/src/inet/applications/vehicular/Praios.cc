//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 


#include "Praios.h"

#include "CAMPacket_m.h"
#include "inet/physicallayer/wireless/common/base/packetlevel/FlatReceiverBase.h"

#include "inet/physicallayer/wireless/common/contract/packetlevel/IRadioMedium.h"
#include "inet/physicallayer/wireless/common/contract/packetlevel/SignalTag_m.h"
#include <cmath>
#define UPDATE_BR_TO 101
#define UPDATE_WS_TO 102





namespace inet {
Define_Module(Praios);
simsignal_t Praios::powerSignal=SIMSIGNAL_NULL;
simsignal_t Praios::totalReceivedRate=SIMSIGNAL_NULL;
simsignal_t Praios::emitedRate=SIMSIGNAL_NULL;
simsignal_t Praios::subgradientSignal=SIMSIGNAL_NULL;
simsignal_t Praios::totalWeightSignal=SIMSIGNAL_NULL;
simsignal_t Praios::myWeightSignal=SIMSIGNAL_NULL;




Praios::Praios():
                                     updateRateTimer(nullptr),
                                     updateWeightTimer(nullptr)

{
}
Praios::~Praios() {
    cancelAndDelete(updateRateTimer);
    cancelAndDelete(updateWeightTimer);

}

void Praios::initialize(int stage)
{
    CAMGenerator::initialize(stage);
    if (stage==INITSTAGE_LOCAL) {
        weight=par("weight");
        MBL_bps =par("MBL_bps");
        MBL_cbt=par("MBL_cbt");
        receivedRate=0.0;
        subGradient=0.0;
        synchronous=par("synchronous");
        syncPeriod = par("syncPeriod");
        useSplit = par("useSplit");
        useLocalGradMod=par("useLocalGradMod");
        alpha = par("alpha");
        minWeight=0.0;
        useCBT= par("useCBT");
        priority=par("priority");
        gammaStep= par("gammaStep");
        betaStep = par("betaStep");
        const char *vstr = par("powers").stringValue();
        powerRange= cStringTokenizer(vstr).asDoubleVector();
        currentPower = par("initialPower");
        //currentPower = powerRange[1]*0.5;
        const char *vstr2 = par("rates").stringValue();
        beconRateRange = cStringTokenizer(vstr2).asDoubleVector();
        beaconRate= beconRateRange[1];
        beaconTime=1.0/beaconRate;
        CAMreceived=0;
        CAMsent=0;
        if (beconRateRange.size()>2 || powerRange.size()>2) {
            error("Power and rate limits can only have two values");
        }

        hmin=1.0/powerRange[1];
        hmax = 1.0/powerRange[0];
        ymax=log(beconRateRange[1]);
        ymin=log(beconRateRange[0]);

        h=hmin;
        y=ymax; //Max rate
        epsilon = par("epsilon");
        hpathloss = par("pathLoss");
        hpathloss=hpathloss/2.0;
        updateWeightTimer=new cMessage("UPDATE_WS_TO",UPDATE_WS_TO);
        updateRateTimer = new cMessage("UPDATE_BR_TO",UPDATE_BR_TO);
        disableControl=par("disableControl");
        if (disableControl) {
            EV<<"Control of rate and power is disabled"<<endl;
        } else {
            if (!disableGeneration) {
                if (useSplit) {
                    if (synchronous) {
                        simtime_t in=simTime().trunc(SIMTIME_S) + syncPeriod/2.0;

                        scheduleAt(in, updateWeightTimer);
                    } else {
                        scheduleAt(simTime()+uniform(0,syncPeriod), updateWeightTimer);
                    }
                } else {
                    if (synchronous) {
                        simtime_t in=simTime().trunc(SIMTIME_S) + syncPeriod;

                        scheduleAt(in, updateRateTimer);
                    } else {
                        scheduleAt(simTime()+uniform(0,syncPeriod), updateRateTimer);
                    }
                }
            }
        }



        if (useCBT) { //If using CBT, it has to be measured everytime the gradient is computed
            if (cbfSampleTimer->isScheduled()) {
                    cancelEvent(cbfSampleTimer);
            }
        }







        //totalReceivedRate=registerSignal("totalReceivedRate");
        //emitedRate=registerSignal("emitedRate");
        subgradientSignal=registerSignal("subgradient");
        myWeightSignal=registerSignal("myWeight");
        //totalWeightSignal=registerSignal("totalWeight");



        powerSignal=registerSignal("power");
        WATCH(h);
        WATCH(y);
        WATCH(hmin);
        WATCH(ymin);
        WATCH(hmax);
        WATCH(ymax);
        WATCH(currentPower);

        WATCH(weight);


    } else if (stage==INITSTAGE_APPLICATION_LAYER) {


        const physicallayer::FlatReceiverBase* fr= check_and_cast<const physicallayer::FlatReceiverBase*>((radio->getReceiver()));
        W sensitivity=fr->getSensitivity();
        Hz f = fr->getCenterFrequency();
        const physicallayer::IRadioMedium *radioMedium = check_and_cast<const physicallayer::IRadioMedium *>(getParentModule()->getParentModule()->getModuleByPath(".radioMedium"));
        mps c=radioMedium->getPropagation()->getPropagationSpeed();
        double A=(4*M_PI*4*M_PI)*f.get()*f.get()/(c.get()*c.get());
        double dt = par("targetDistance");
        W Dth=mW(math::dBmW2mW(par("Dth")));
        //double thW= pow(10.0, Dth.get() / 10.0); //Working in W directly (sensitivity is also in W)
        double thW=Dth.get();
        if (sensitivity>Dth) {
            thW=sensitivity.get();
            EV<<"WARNING: Dth is lower ("<<Dth<<" than sensitivity ("<<sensitivity<<". Using sensitivity"<<endl;
        }

        Ko= thW*A*1*pow(dt,hpathloss*2.0);
        WATCH(Ko);
        //SAm=sensitivity.get()*A*1;
        SAm=thW*A*1; //use Decoding threshold

        WATCH(SAm);
        // TODO: THIS STRONGLY DEPENDS ON THE DATARATE, SHOULD BE ASKED TO THE PHY LAYER...But is not easy with the current API...
        //The basic encapsulation is
        // PHY Header (5) | MAC HEADER (24) | LLCEDP (2) | CAM | MAC TRAILER (4) | PHY TRAILER (2)
        //But PHY Trailer depends on the length of the CAM via the padding...Also PHY Header duration depends on the datarate
        camtime=752e-6;

        //C=((double)MBL_bps)/((beaconSize+40)*8);//Including MAC and PHY layer overhead for 6Mbps datarate and only with 500Byte beacons + 40 B of MAC headers
        //Much more clear to just use a fraction
        channelCapacityMS = 1.0/camtime;
        C=MBL_cbt*channelCapacityMS;
        WATCH(C);

        Cfraction=C*camtime;
        std::cout<<myId<<":"<<"f="<<f<<"; c="<<c<<"; A="<<A<<" Dth="<<Dth<<"; thW="<<thW<<"; Ko="<<Ko<<"; C="<<C<<"; Cfraction="<<Cfraction<<";channel capacity (message/s)"<<channelCapacityMS<<std::endl;
        EV<<myId<<":"<<"f="<<f<<"; c="<<c<<"; A="<<A<<" Dth="<<Dth<<"; thW="<<thW<<"; Ko="<<Ko<<"; C="<<C<<"; Cfraction="<<Cfraction<<";channel capacity (message/s)"<<channelCapacityMS<<std::endl;
        lastCBT=0.0;
        emit(powerSignal,currentPower);


    }

}
void Praios::receiveSignal(cComponent *source, simsignal_t signalID, double d) {

    if (signalID==changeMinBeaconRateSignal) {
        std::cout<<myId<<"receive signal, new minrate="<<d<<endl;
        if (d>0) {
            ymin=log(d);
        } else {
            ymin=log(beconRateRange[0]);
        }
    }
    if (signalID==changeMinPowerSignal) {
        if (d>0) {
            std::cout<<myId<<"receive signal, new power="<<d<<endl;
            hmax=1.0/d;
        } else {
            hmax = 1.0/powerRange[0];
        }
    }

}

void Praios::handleMessage(cMessage *msg)
{
    if (msg->isSelfMessage()) {

        if (msg->getKind()==UPDATE_BR_TO) {
            if (useSplit) {
                updateRateAndPower();
                scheduleAt(simTime()+syncPeriod/2, updateWeightTimer);
            } else {


                computeSubgradient();
                updateWeight();
                updateRateAndPower();
                scheduleAt(simTime()+syncPeriod, updateRateTimer);

            }
        } else  if (msg->getKind()==UPDATE_WS_TO) {
            computeSubgradient();

            updateWeight();

            scheduleAt(simTime()+syncPeriod/2, updateRateTimer);
        } else if (msg->getKind()==CAM_GENERATION_TO) {

            sendCAMAtPower(beaconRate,currentPower);


            scheduleAt(simTime()+(beaconTime),msg);

        } else {
            CAMGenerator::handleMessage(msg);
        }

    } else {
        CAMGenerator::handleMessage(msg);
    }
}
void Praios::sendCAMAtPower(double br, double p) {
    computePDR();
    auto data = makeShared<CAMPacket>();
    data->setSource(myId);
    data->setCreationTime(simTime());
    data->setSequenceNumber(CAMsent);
    data->setChunkLength(B(beaconSize));
    data->setPosition(mob->getCurrentPosition());
    data->setSpeed(mob->getCurrentVelocity());
    data->setAngle(0.0);
    data->setBeaconRate(beaconRate);
    data->setWeight(weight);
    data->setPower(p);

    Packet* cam = new Packet("cam", data);

    //Indicate required power to physical layer
    cam->addTagIfAbsent<SignalPowerReq>()->setPower(W(p));

    sendDown(cam);
    CAMsent++;


}
void  Praios::updateWeight() {
    weight = weight +  betaStep*subGradient;

    //std::cout<<simTime()<<"--"<<myId<<"weight="<<weight<<endl;
    if (weight<0) {
        weight=0;
    }
    emit(myWeightSignal,weight);

}
void Praios::computeSubgradient() {
    receivedRate=beaconRate; //exp(y)=exp(log(r))
    // double msaux=beaconRate;
    if (useCBT) {
        double measuredCBT=getMeasuredCBT(syncPeriod);

        subGradient = (measuredCBT -Cfraction)/camtime;
        //std::cout<<myId<<":"<<simTime()<<"--"<<"mcbt="<<measuredCBT<<"lcbt="<<lastCBT<<"deltacbt="<<measuredCBT-lastCBT<<"sg="<<subGradient<<endl;
        lastCBT=measuredCBT;
    } else {
        VTable::iterator it = vehicleTable->vt.begin();
        while (it !=vehicleTable->vt.end()) {
            double Kvvh=SAm*(pow(mob->getCurrentPosition().sqrdist(it->second->pos),hpathloss))/(it->second->power);
            receivedRate += (it->second->beaconRate)*exp(-Kvvh);
            // std::cout<<simTime()<<"--"<<myId<<"bre="<<(it->second->beaconRate)*exp(-Kvvh)<<"bra="<<(it->second->beaconRate)<<"mbr="<<mbre<<endl;
            //sqrReceivedRate += ((it->second->beaconRate)*(it->second->beaconRate));
            ++it;

        }
        //std::cout<<myId<<":"<<simTime()<<"--sg2="<<(receivedRate-C)<<std::endl;
        subGradient=receivedRate-C; //sum -C
    }
    emit(subgradientSignal,subGradient);

}
void Praios::updateRateAndPower(){

    if (useLocalGradMod) {

        localGradientProjectionMod();
    } else {
        localGradientProjection();
    }

    //localGradientProjectionRegularized();

    //We have updated our rate and power
    double myRate = exp(y);
    currentPower = 1.0/h;
    emit(powerSignal,currentPower);
    if (beaconGenerationTimer->isScheduled()) {
        rescheduleBeacon(myRate);
    }
    setBeaconRate(myRate);
}
//Compute just the h step and solve the y value
void Praios::localGradientProjectionMod() {



    double hprev=1e6;
    double gstep=gammaStep;
    std::vector<double> Kvv;
    VTable::iterator it = vehicleTable->vt.begin();
    double sw=weight;
    //Fill vector
    while (it !=vehicleTable->vt.end()) {

        Kvv.push_back(SAm*(pow(mob->getCurrentPosition().sqrdist(it->second->pos),hpathloss)));
        sw += (it->second->weight);
        ++it;
    }

    int i=1;
    while (true) {


        //double grady=weight*beaconRate; //exp(y)=exp(log(beaconRate)); //My own rate
        double gradh=0.0; //My own probability
        double ek=priority*pow(exp((y-Ko*h)),(1.0-alpha));

        VTable::iterator it = vehicleTable->vt.begin();

        double aux =0.0;

        int v=0;
        while (it !=vehicleTable->vt.end()) {

            //double Kvv=SAm*(pow(mob->getCurrentPosition().sqrdist(it->second->pos),hpathloss));
            double ekvh=exp(-Kvv[v]*h);
            aux += ekvh*(it->second->weight);
            double aux2 = Kvv[v]*exp(y)*ekvh*(it->second->weight);
            //auxd2h += aux2*Kvv[v];
            gradh += aux2;
            ++it;
            ++v;

        }
        //To speed up convergence
        //Atencion, debe estar antes que grady se actualice abajo
        //double d2y = std::abs(-ek*(1.0-alpha)+grady);
        //double d2h = std::abs(Ko*Ko*ek*(1.0-alpha)-auxd2h);

        //As minimization of -U
        //grady = -ek+grady;
        gradh = Ko*ek - gradh;

        //y = y + (epsilon/(i))*(grady/d2y);
        //h = h + (epsilon/(i))*(gradh/d2h);

        //As minimization substract
        //y = y - (gammaStep*grady);

        //Use decreasing step
        if (i%100==0) {
            gstep=gstep*0.9;
        }

        h = h - (gstep*gradh);
        //Now we use the exact y
        y=((Ko*h*(alpha-1) +log(priority) -log(aux)))/alpha;

        //Projection on allowable set
        if (y<ymin) {
            y=ymin;
        } else if (y>ymax) {
            y=ymax;
        }
        if (h<hmin) {
            h=hmin;
        } else if (h>hmax) {
            h=hmax;
        }

        double dp = std::abs(h-hprev);
        // double dp = (h-hprev)*(h-hprev) +(y-yprev)*(y-yprev);
        if (dp<1e-8) {

                break;

        }

        ++i;

        hprev=h;

    }
}
void Praios::localGradientProjection() {

    double yprev=1e6;
    double hprev=1e6;
    double gstep=gammaStep;
    std::vector<double> Kvv;
    VTable::iterator it = vehicleTable->vt.begin();
    double sw=weight;
    //Fill vector
    while (it !=vehicleTable->vt.end()) {

        Kvv.push_back(SAm*(pow(mob->getCurrentPosition().sqrdist(it->second->pos),hpathloss)));
        sw += (it->second->weight);
        ++it;
    }

    int i=1;
    while (true) {


        double grady=weight*beaconRate; //exp(y)=exp(log(beaconRate)); //My own rate
        double gradh=0.0; //My own probability
        double ek=priority*pow(exp((y-Ko*h)),(1.0-alpha));

        VTable::iterator it = vehicleTable->vt.begin();

        double auxd2h =0.0;

        int v=0;
        while (it !=vehicleTable->vt.end()) {

            //double Kvv=SAm*(pow(mob->getCurrentPosition().sqrdist(it->second->pos),hpathloss));
            double aux=exp(y-Kvv[v]*h);
            grady += aux*(it->second->weight);
            double aux2 = Kvv[v]*aux*(it->second->weight);
            auxd2h += aux2*Kvv[v];
            gradh += aux2;
            ++it;
            ++v;

        }
        //To speed up convergence
        //Atencion, debe estar antes que grady se actualice abajo
        //double d2y = std::abs(-ek*(1.0-alpha)+grady);
        //double d2h = std::abs(Ko*Ko*ek*(1.0-alpha)-auxd2h);

        //As minimization of -U
        grady = -ek+grady;
        gradh = Ko*ek - gradh;

        //y = y + (epsilon/(i))*(grady/d2y);
        //h = h + (epsilon/(i))*(gradh/d2h);
        //Use decreasing step
               if (i%100==0) {
                   gstep=gstep*0.9;
               }
        //As minimization substract
        y = y - (gstep*grady);
        h = h - (gstep*gradh);

        /*
               if (myId==98) {
                   std::cout<<myId<<"--y="<<y<<"h="<<h<<"n="<<vehicleTable->vt.size()<<endl;
               }*/
        //Projection on allowable set
        if (y<ymin) {
            y=ymin;
        } else if (y>ymax) {
            y=ymax;
        }
        if (h<hmin) {
            h=hmin;
        } else if (h>hmax) {
            h=hmax;
        }

        double dp = std::abs(h-hprev);
        // double dp = (h-hprev)*(h-hprev) +(y-yprev)*(y-yprev);
        if (dp<1e-7) {
            double dr = std::abs(y-yprev);
            if (dr<1e-7) {



                //if (myId==90) {
                //  std::cout<<simTime()<<"--y="<<y<<"h="<<h<<"dh="<<0.5/(Ko*(-(grady-ek))/exp(y))<<endl;
                //std::cout<<"--------weight="<<weight<<"sw="<<sw<<"sumw="<<sumWeights<<"deltasuum="<<sw-sumWeights<<endl;
                //sumWeights=sw;
                //}
                break;
            }
        }

        ++i;
        yprev=y;
        hprev=h;

    }
}

void Praios::localGradientProjectionRegularized() {

    double yprev=1e6;
    double hprev=1e6;
    std::vector<double> Kvv;
    VTable::iterator it = vehicleTable->vt.begin();
    double sw=weight;
    //Fill vector
    while (it !=vehicleTable->vt.end()) {

        Kvv.push_back(SAm*(pow(mob->getCurrentPosition().sqrdist(it->second->pos),hpathloss)));
        sw += (it->second->weight);
        ++it;
    }

    int i=1;
    while (true) {


        double grady=weight*beaconRate; //exp(y)=exp(log(beaconRate)); //My own rate
        double gradh=0.0; //My own probability
        double ek=priority*pow(exp((y-Ko*h)),(1.0-alpha));

        VTable::iterator it = vehicleTable->vt.begin();

        double auxd2h =0.0;

        int v=0;
        while (it !=vehicleTable->vt.end()) {

            //double Kvv=SAm*(pow(mob->getCurrentPosition().sqrdist(it->second->pos),hpathloss));
            double aux=exp(y-Kvv[v]*h);
            grady += aux*(it->second->weight);
            double aux2 = Kvv[v]*aux*(it->second->weight);
            auxd2h += aux2*Kvv[v];
            gradh += aux2;
            ++it;
            ++v;

        }
        //To speed up convergence
        //Atencion, debe estar antes que grady se actualice abajo
        //double d2y = std::abs(-ek*(1.0-alpha)+grady);
        //double d2h = std::abs(Ko*Ko*ek*(1.0-alpha)-auxd2h);

        //As minimization of -U
        grady = -ek+grady + 2*epsilon*y;
        gradh = Ko*ek - gradh + 2*epsilon*h;

        //y = y + (epsilon/(i))*(grady/d2y);
        //h = h + (epsilon/(i))*(gradh/d2h);
        //As minimization substract
        y = y - (gammaStep*grady);
        h = h - (gammaStep*gradh);

        /*
               if (myId==98) {
                   std::cout<<myId<<"--y="<<y<<"h="<<h<<"n="<<vehicleTable->vt.size()<<endl;
               }*/
        //Projection on allowable set
        if (y<ymin) {
            y=ymin;
        } else if (y>ymax) {
            y=ymax;
        }
        if (h<hmin) {
            h=hmin;
        } else if (h>hmax) {
            h=hmax;
        }

        double dp = std::abs(h-hprev);
        // double dp = (h-hprev)*(h-hprev) +(y-yprev)*(y-yprev);
        if (dp<1e-7) {
            double dr = std::abs(y-yprev);
            if (dr<1e-7) {



                //if (myId==90) {
                //  std::cout<<simTime()<<"--y="<<y<<"h="<<h<<"dh="<<0.5/(Ko*(-(grady-ek))/exp(y))<<endl;
                //std::cout<<"--------weight="<<weight<<"sw="<<sw<<"sumw="<<sumWeights<<"deltasuum="<<sw-sumWeights<<endl;
                //sumWeights=sw;
                //}
                break;
            }
        }



        ++i;
        yprev=y;
        hprev=h;




    }
}







} //namespace
