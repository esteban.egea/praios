//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "CAMGenerator.h"
#include "CAMPacket_m.h"
#include "VehicleTable.h"
#include "inet/common/ModuleAccess.h"
#include "inet/linklayer/common/MacAddressTag_m.h"
#include "inet/linklayer/common/Ieee802SapTag_m.h"
#include "inet/linklayer/common/MacAddress.h"
#include "inet/common/packet/chunk/ByteCountChunk.h"
#include "inet/common/Ptr.h"
#include "inet/common/ProtocolGroup.h"
#include "inet/common/ProtocolTag_m.h"
#include "inet/common/Simsignals.h"
#include "inet/physicallayer/wireless/common/contract/packetlevel/IRadioMedium.h"
#define CBF_SAMPLE_TO 103

namespace inet {

Define_Module(CAMGenerator);

MaxDif::MaxDif(int window) {     windowSize=window; max=-100000; min = 100000; samples=0;}
void MaxDif::reset() {max=-100000; min = 100000; samples=0;}
bool MaxDif::addSample(double s) {
    if (s>max) {
        max=s;
    }
    if (s<min) {
        min=s;
    }
    samples+=1;
    if (samples==windowSize) {
        return true;
    } else {
        return false;
    }
}



std::string  MaxDif::toString() const{
    std::ostringstream s(std::ostringstream::out);


    s<<"max="<<this->max<<"min="<<this->min<<"samples="<<this->samples;
    return s.str();
}

std::ostream& operator<<(std::ostream& out,const MaxDif& obj) {
    std::string a=obj.toString();
    out<<a;
    return (out);

}



simsignal_t CAMGenerator::beaconRateSignal =SIMSIGNAL_NULL;
simsignal_t CAMGenerator::diff_maxSignal =SIMSIGNAL_NULL;
simsignal_t CAMGenerator::sqr_mRateSignal =SIMSIGNAL_NULL;
simsignal_t CAMGenerator::changeMinBeaconRateSignal =SIMSIGNAL_NULL;
simsignal_t CAMGenerator::changeMinPowerSignal =SIMSIGNAL_NULL;
simsignal_t CAMGenerator::camReceivedSignal =SIMSIGNAL_NULL;
simsignal_t CAMGenerator::cbtSignal = SIMSIGNAL_NULL;

CAMGenerator::CAMGenerator() {
    beaconGenerationTimer=nullptr;
    currentTransmission=nullptr;
    cbfSampleTimer=nullptr;

}
CAMGenerator::~CAMGenerator() {
    cancelAndDelete(beaconGenerationTimer);
    cancelAndDelete(cbfSampleTimer);
    delete info;
}
void CAMGenerator::initialize(int stage)
{
    if (stage==INITSTAGE_LOCAL) {
        lowerLayerIn = findGate("socketIn");
        lowerLayerOut = findGate("socketOut");
        beaconRateSignal=registerSignal("brate");
        //diff_maxSignal= registerSignal("diff_max");
        //sqr_mRateSignal=registerSignal("sqr_m");
        changeMinBeaconRateSignal=registerSignal("changeBeaconRate");
        changeMinPowerSignal=registerSignal("changePower");
        camReceivedSignal=registerSignal("camReceived");

        myId=getParentModule()->par("id");
        beaconRate=par("beaconRate");
        beaconTime = 1.0/beaconRate;
        setBeaconRate(beaconRate);
        channelBitRate=par("channelBitRate");
        maxBeaconRate=par("maxBeaconRate");
        minBeaconRate=par("minBeaconRate");
        diffWindow=par("diffWindow");
        beaconSize=par("beaconSize");
        disableGeneration=par("disableGeneration");

        beaconGenerationTimer = new cMessage("Beacon Timer", CAM_GENERATION_TO);

        CAMreceived=0;
        CAMsent=0;
        const char *pdrs = par("pdrs").stringValue();
        pdrRanges=cStringTokenizer(pdrs).asDoubleVector();
        pdrSignals.resize(pdrRanges.size());
        for (unsigned int i=0; i<pdrRanges.size();i++) {
            std::string sname("pdr");
            sname+=std::to_string(pdrRanges[i]);
            pdrSignals[i]=registerSignal(sname.c_str());
            pdrRanges[i]=pdrRanges[i]*pdrRanges[i];
            cProperty *statisticTemplate =
                    getProperties()->get("statisticTemplate", "pdr");
            getEnvir()->addResultRecorders(this, pdrSignals[i], sname.c_str(), statisticTemplate);

        }
        pdrAtDistance.resize(pdrRanges.size());
        currentTransmissionSeqNumber=-1;
        mdiff=MaxDif(diffWindow);
        WATCH(mdiff);
        WATCH(beaconRate);
        jitter=par("jitter");
        Coord pos(-1,-1);
        Coord speed(-1,-1);
        info= new VehicleInfo(-1,pos,speed,-1,-1,1000);
        initTime=par("initTime");

        mob = getModuleFromPar<IMobility>(par("mobilityModule"),getParentModule());
        vehicleTable = getModuleFromPar<VehicleTable>(par("vehicleTable"), getParentModule());

        cbfSampleTimer = new cMessage("CBT TO", CBF_SAMPLE_TO);
        cbtWindow=par("cbtWindow");
        setCbtWindow(cbtWindow);
        cbtSignal = registerSignal("cbt");

    } else if (stage==INITSTAGE_APPLICATION_LAYER) {
        radio=check_and_cast<physicallayer::Radio*>(getParentModule()->getModuleByPath(".wlan.radio"));
        getParentModule()->subscribe(changeMinBeaconRateSignal,this);
        getParentModule()->subscribe(changeMinPowerSignal,this);

        //CBT measurement
        radio->subscribe(physicallayer::IRadio::receptionStateChangedSignal, this);
        radio->subscribe(physicallayer::IRadio::transmissionStateChangedSignal, this);
        lastReceptionState=radio->getReceptionState();
        lastTransmissionState=radio->getTransmissionState();
        cbt_rtime=0.0;
        lu_rtime=simTime();
        cbt_txtime=0.0;
        lu_txtime=simTime();
        cbt_idletime=0.0;
        lu_idletime=simTime();
        //To compute PDR
        radio->subscribe(transmissionStartedSignal, this);
        getParentModule()->getParentModule()->subscribe(physicallayer::IRadioMedium::signalArrivalStartedSignal,this);
        getParentModule()->getParentModule()->subscribe(physicallayer::IRadioMedium::signalRemovedSignal, this);
        getSimulation()->getSystemModule()->subscribe(camReceivedSignal, this);
        simtime_t nb= simTime()+ uniform(0,beaconTime);
        if (!disableGeneration) {
            scheduleAt(nb,beaconGenerationTimer);
        }
        numberOfNodes=getParentModule()->getParentModule()->getSubmoduleVectorSize("node");
        sentCAMReceivedSignals=0;
        // recordScalar("initialX",mob->par("initialX"));
        // recordScalar("initialY",mob->par("initialY"));
        //endSimulation();
        WATCH(numberOfNodes);

    }
}
void CAMGenerator::handleMessage(cMessage *msg)
{
    if (simTime()>=initTime) {
        if (msg->getArrivalGateId()==lowerLayerIn) { //New CAM

            // std::cout<<"Received CAM"<<std::endl;
            handleCAM(msg);
            delete msg;


        } else if (msg->isSelfMessage()) {
            if (msg->getKind()==CAM_GENERATION_TO) {
                sendCAM();
                if (jitter>0) {
                    if (beaconTime > 5*jitter) {
                        scheduleAt(simTime()+beaconTime + intuniform(-5,5)*jitter,beaconGenerationTimer);
                    } else {
                        scheduleAt(simTime()+beaconTime,beaconGenerationTimer);
                    }
                } else {
                    scheduleAt(simTime()+beaconTime,beaconGenerationTimer);
                }

            }
            if (msg->getKind()==CBF_SAMPLE_TO) {

                getMeasuredCBT(cbtWindow.dbl());
                scheduleAt(simTime()+cbtWindow,cbfSampleTimer);
                //cbt_rtime=0.0;
            }
        }
    } else {
        //Might be problems
        delete msg;
    }

}

void CAMGenerator::receiveSignal(cComponent *source, simsignal_t signal, intval_t value, cObject *details) {
    //CBT
        if (signal == physicallayer::IRadio::receptionStateChangedSignal) {

            //It is actually easier to compute the CBT with the IDLE state. Anything that is not idle must be busy somehow (transmitting, receiving or busy)
            physicallayer::IRadio::ReceptionState newState=static_cast<physicallayer::IRadio::ReceptionState>(value);
            if (newState!= physicallayer::IRadio::ReceptionState::RECEPTION_STATE_IDLE && lastReceptionState ==physicallayer::IRadio::ReceptionState::RECEPTION_STATE_IDLE) {


                cbt_idletime+=(simTime()-lu_idletime);
            }
            if (newState==physicallayer::IRadio::ReceptionState::RECEPTION_STATE_IDLE) {
                lu_idletime=simTime();
            }

            //if (newState!= physicallayer::IRadio::ReceptionState::RECEPTION_STATE_RECEIVING && lastReceptionState ==physicallayer::IRadio::ReceptionState::RECEPTION_STATE_RECEIVING) {
            if ((newState!= physicallayer::IRadio::ReceptionState::RECEPTION_STATE_RECEIVING || newState!=physicallayer::IRadio::ReceptionState::RECEPTION_STATE_BUSY) && (lastReceptionState ==physicallayer::IRadio::ReceptionState::RECEPTION_STATE_RECEIVING || newState==physicallayer::IRadio::ReceptionState::RECEPTION_STATE_BUSY )) {

                //We were receiving
                cbt_rtime+=(simTime()-lu_rtime);
            }
            if (newState==physicallayer::IRadio::ReceptionState::RECEPTION_STATE_RECEIVING || newState==physicallayer::IRadio::ReceptionState::RECEPTION_STATE_BUSY  ) {
                lu_rtime=simTime();
            }
            lastReceptionState=newState;
            //What about IDLE and BUSY
        }


        if (signal == physicallayer::IRadio::transmissionStateChangedSignal) {
            physicallayer::IRadio::TransmissionState newState=static_cast<physicallayer::IRadio::TransmissionState>(value);
            if (newState!= physicallayer::IRadio::TransmissionState::TRANSMISSION_STATE_TRANSMITTING && lastTransmissionState ==physicallayer::IRadio::TransmissionState::TRANSMISSION_STATE_TRANSMITTING) {
                //We were receiving
                cbt_txtime+=(simTime()-lu_txtime);
            }
            if (newState==physicallayer::IRadio::TransmissionState::TRANSMISSION_STATE_TRANSMITTING) {
                lu_txtime=simTime();

            }
            lastTransmissionState=newState;
        }
}
void CAMGenerator::receiveSignal(cComponent *source, simsignal_t signal, cObject *obj, cObject *details) {
    if (signal==transmissionStartedSignal) {
        //Our transmission has started
        currentTransmission=check_and_cast< const physicallayer::ITransmission* >(obj);
        const Packet* pkt=currentTransmission->getPacket();
        auto p=pkt->peekDataAt<CAMPacket>(B(31)); //PhyHeader(5)+MACHeader(24)+LLCEDP(2)
        currentTransmissionSeqNumber=p->getSequenceNumber();
        sentCAMReceivedSignals=0;
        numberOfNodes=getParentModule()->getParentModule()->getSubmoduleVectorSize("node");
        //std::cout<<myId<<"tx started ctsn="<<currentTransmissionSeqNumber<<p->getSequenceNumber()<<std::endl;


    }
    if (signal==physicallayer::IRadioMedium::signalRemovedSignal) {
        const physicallayer::ITransmission* t=check_and_cast<const physicallayer::ITransmission*>(obj);
        if (currentTransmission) {
            if (currentTransmission->getId()==t->getId()) {
                currentTransmission=nullptr;
                //currentTransmissionSeqNumber=-1;
                sentCAMReceivedSignals=0;
            }
        }
    }
    if (signal==physicallayer::IRadioMedium::signalArrivalStartedSignal) {
        const physicallayer::IReception* rec=check_and_cast<const physicallayer::IReception*>(obj);
        sentCAMReceivedSignals++;
        if (currentTransmission) {
            const physicallayer::ITransmission* t= rec->getTransmission();
            //std::cout<<myId<<std::endl;
            if (t) {
                int idt=t->getId();
                int cid=currentTransmission->getId();
                if (idt==cid) {
                    double sqrd=mob->getCurrentPosition().sqrdist(rec->getStartPosition());
                    for (unsigned int i=0; i<pdrAtDistance.size(); i++) {

                        if (sqrd<=pdrRanges[i]) {
                            pdrAtDistance[i].vehicles++;
                            //std::cout<<myId<<":"<<simTime()<<"PDR-"<<i<<" arrivalStarted from"<<rec->getStartPosition()<<"d="<<sqrd<<"veh="<<pdrAtDistance[i].vehicles<<std::endl;
                        }
                    }
                }
            }
        }
        if (sentCAMReceivedSignals==numberOfNodes) {
            currentTransmission=nullptr;
        }
    }
    if (signal==camReceivedSignal) {
        CAMReceivedInfo* info=check_and_cast<CAMReceivedInfo*>(obj);
        if (info->sequenceNumber==currentTransmissionSeqNumber && info->source==myId) {
            double sqrd=mob->getCurrentPosition().sqrdist(info->position);
            //std::cout<<myId<<"source="<<info->id<<"sn="<<info->sequenceNumber<<"ctsn="<<currentTransmissionSeqNumber<<"sqrd="<<sqrd<<std::endl;
            for (unsigned int i=0; i<pdrAtDistance.size(); i++) {
                //std::cout<<myId<<"pdrRanges"<<pdrRanges[i]<<std::endl;
                if (sqrd<=pdrRanges[i]) {
                    pdrAtDistance[i].received++;
                    //std::cout<<myId<<":"<<simTime()<<"PDR-packet received from"<<info->id<<" at d="<<sqrd<<" rec="<<pdrAtDistance[i].received<<std::endl;
                }
            }
        }
        //else {
        //    std::cout<<myId<<"source="<<info->id<<"sn="<<info->sequenceNumber<<"ctsn="<<currentTransmissionSeqNumber<<std::endl;
        //}
    }



}
void CAMGenerator::computePDR() {
    for (unsigned int i=0; i<pdrAtDistance.size(); i++) {
        if (pdrAtDistance[i].vehicles>0) {
            emit(pdrSignals[i],((double)pdrAtDistance[i].received/pdrAtDistance[i].vehicles));
        }
        pdrAtDistance[i].vehicles=0;
        pdrAtDistance[i].received=0;

    }
    currentTransmission=nullptr;
    currentTransmissionSeqNumber=-1;
    sentCAMReceivedSignals=0;
}
void CAMGenerator::sendCAM() {
    computePDR();
    auto data = makeShared<CAMPacket>();
    data->setSource(myId);
    data->setSequenceNumber(CAMsent);
    data->setCreationTime(simTime());
    data->setChunkLength(B(beaconSize));
    data->setPosition(mob->getCurrentPosition());
    data->setSpeed(mob->getCurrentVelocity());
    data->setAngle(0.0);
    data->setBeaconRate(beaconRate);

    //data->setByteLength(beaconSize);

    Packet* cam = new Packet("cam", data);

    cam->setKind(CAM_PKT);

    sendDown(cam);
    CAMsent++;


}
void CAMGenerator::sendDown(Packet* p) {
    //Add SAP. I think we may remove this
    p->addTagIfAbsent<Ieee802SapReq>()->setDsap(SapCode::SAP_IP);
    p->addTagIfAbsent<MacAddressReq>()->setDestAddress(MacAddress::BROADCAST_ADDRESS);
    //Should put something sensible here. Keep this to prevent LlcEpd from complaining
    p->addTagIfAbsent<PacketProtocolTag>()->setProtocol(&Protocol::ipv4);


    send(p,lowerLayerOut);
}
void CAMGenerator::setBeaconRate(double br) {



    beaconRate=br;
    beaconTime=1/beaconRate;
    emit(beaconRateSignal,br);
    /* emit(sqr_mRateSignal,br*br);

    if (mdiff.addSample(br)) {
              emit(diff_maxSignal,mdiff.max-mdiff.min);
              mdiff.reset();
          }
     */
}
void CAMGenerator::handleCAM(cMessage* msg) {
    //CAMPacket* p = check_and_cast<CAMPacket*>(msg);
    //VehicleInfo* info= new VehicleInfo(p->getSource(),p->getPosition(),p->getSpeed(),p->getBeaconRate(),p->getWeight(),1000);
    EV_INFO<<myId<<":Received CAM"<< endl;
    Packet* pkt=static_cast<Packet*>(msg);
    auto p=pkt->peekAtFront<CAMPacket>(B(beaconSize));

    info->id=p->getSource();
    info->pos=p->getPosition();
    info->speed=p->getSpeed();
    info->beaconRate=p->getBeaconRate();
    info->weight=p->getWeight();
    info->power=p->getPower();
    vehicleTable->insertOrUpdate(info);
    CAMReceivedInfo* ci=new CAMReceivedInfo();
    ci->id=myId;
    ci->sequenceNumber=p->getSequenceNumber();
    ci->source=p->getSource();
    ci->position=mob->getCurrentPosition();
    emit(camReceivedSignal, check_and_cast<const cObject *>(ci));
    CAMreceived++;
    delete ci;


}

void CAMGenerator::rescheduleBeacon(double rate) {

    // std::cout<<myId<<"Current br="<<beaconRate<<"New br="<<rate<<endl;

    if (beaconRate==rate){
        return;
    }
    simtime_t rest=beaconGenerationTimer->getArrivalTime()-simTime();
    simtime_t newbt=1/rate;
    simtime_t dif=newbt-beaconTime;
    //TODO COMPROBAR ESTO DE NUEVO


    //std::cout<<myId<<"rescheduling CAM at"<<beaconGenerationTimer->getArrivalTime()<<"rest="<<rest<<"dif="<<dif<<endl;
    if (rate<=beaconRate) {
        //This is OK
        cancelEvent(beaconGenerationTimer);

        scheduleAt(simTime()+rest+dif, beaconGenerationTimer);

        //std::cout<<myId<<"rescheduling CAM for"<<(simTime()+rest+dif)<<endl;

    } else {
        dif=-dif;

        if (rest > dif){
            //it is not time to send the new beacon yet. OK here
            cancelEvent(beaconGenerationTimer);

            scheduleAt(simTime()+rest-dif, beaconGenerationTimer);

            //std::cout<<myId<<"rescheduling CAM for"<<(simTime()+rest+dif)<<endl;
        } else {
            int d = floor(rest/newbt);

            if (d>0) {
                cancelEvent(beaconGenerationTimer);

                scheduleAt(simTime()+rest-d*newbt,beaconGenerationTimer);



            }
        }

    }
}
const simtime_t& CAMGenerator::getCbtWindow() const {

    return cbtWindow;
}
void CAMGenerator::setCbtWindow(const simtime_t& cbtWindow, double offset) {
    Enter_Method_Silent();
    this->cbtWindow = cbtWindow;
    if (cbfSampleTimer->isScheduled()) {
        cancelEvent(cbfSampleTimer);
    }
    if (offset>0) {
        scheduleAt(simTime()+offset+cbtWindow,cbfSampleTimer);
    } else {
        scheduleAt(simTime()+cbtWindow,cbfSampleTimer);
    }
}
double CAMGenerator::getMeasuredCBT(double period) {
    if (radio->getReceptionState() == physicallayer::IRadio::ReceptionState::RECEPTION_STATE_IDLE) {
        cbt_idletime +=(simTime()-lu_idletime);
        lu_idletime=simTime();
    }
    if (radio->getReceptionState() == physicallayer::IRadio::ReceptionState::RECEPTION_STATE_RECEIVING || radio->getReceptionState() ==physicallayer::IRadio::ReceptionState::RECEPTION_STATE_BUSY ) {
        //I am in the middle of reception or transmission, update CBT
        cbt_rtime +=(simTime()-lu_rtime);
        //std::cout<<myId<<":"<<simTime().dbl()<<"--middle getMeasuredCBT:lurx="<<lu_rtime<<"cbt_rtime="<<cbt_rtime.dbl()<<std::endl;
        lu_rtime=simTime();

    }
    if (radio->getTransmissionState() == physicallayer::IRadio::TransmissionState::TRANSMISSION_STATE_TRANSMITTING ) {
        cbt_txtime +=(simTime()-lu_txtime);
        //std::cout<<myId<<":"<<simTime().dbl()<<"--middle getMeasuredCBT:lutx="<<lu_txtime<<"cbt_txtime="<<cbt_txtime.dbl()<<std::endl;
        lu_txtime=simTime();
    }

    //double mcbt=(cbt_rtime.dbl()+cbt_txtime.dbl())/period;
    //Work with the idle stae
    double mcbt=1.0 -(cbt_idletime.dbl()/period);
    //std::cout<<myId<<":"<<simTime().dbl()<<"--getMeasuredCBT:"<<mcbt<<"lurx="<<lu_rtime<<"cbt_rtime="<<cbt_rtime.dbl()<<"cbt_txtime="<<cbt_txtime.dbl()<<"lutx="<<lu_txtime<<"idle="<<cbt_idletime<<"luidle="<<lu_idletime<<std::endl;
    cbt_rtime=0.0;
    cbt_txtime =0.0;
    cbt_idletime=0.0;
    emit(cbtSignal,mcbt);
    return mcbt;

}
} //namespace
