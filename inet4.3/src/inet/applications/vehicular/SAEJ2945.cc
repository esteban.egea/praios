//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "SAEJ2945.h"
#include "VehicleTableSAE.h"
#include "inet/physicallayer/wireless/common/contract/packetlevel/SignalTag_m.h"
#include "CAMPacket_m.h"

#define SAE_TXRATE_TO 101
namespace inet {

Define_Module(SAEJ2945);
simsignal_t SAEJ2945::powerSignal=SIMSIGNAL_NULL;

void SAEJ2945::initialize(int stage) {
    CAMGenerator::initialize(stage);
    if (stage==INITSTAGE_LOCAL) {
        vTxRateCntrlInt=par("vTxRateCntrlInt");
        vTxRateCntrlIntTimer = new cMessage("Tx control timer", SAE_TXRATE_TO );
        synchronous=par("synchronous");
        vMaxSuccessiveFail=par("vMaxSuccessiveFail");
        vTrackingErrMin=par("vTrackingErrMin");
        vTrackingErrMax=par("vTrackingErrMax");
        vErrSensitivity=par("vErrSensitivity");
        vDensityCoefficient=par("vDensityCoefficient");
        vDensityWeightFactor=par("vDensityWeightFactor");
        vRescheduleTh = par("vRescheduleTh");
        vMinChanUtil=par("vMinChanUtil");
        vMaxChanUtil=par("vMaxChanUtil");
        vCBPWeightFactor=par("vCBPWeightFactor");
        vSUPRAGain =par("vSUPRAGain");
        vTxRand=par("vTxRand");
        randomizedBeacon = par("randomizedBeacon");
        const char *vstr = par("powers").stringValue();
        powerRange= cStringTokenizer(vstr).asDoubleVector();
        vRPMax = powerRange[powerRange.size()-1];
        vRPMin = powerRange[0];
        const char *vstr2 = par("rates").stringValue();
        beconRateRange = cStringTokenizer(vstr2).asDoubleVector();
        beaconRate= beconRateRange[1];
        vmin_itt=1.0/beconRateRange[1];
        vmax_itt=1.0/beconRateRange[0];
        beaconTime=1.0/beaconRate;

        fails=0;

        lastCBT=0.0;
        lastSentBeaconTime=simTime();
        currentPower = par("initialPower");
        powerSignal=registerSignal("power");
        lastPower = currentPower;
        lastDensity=0.0;

    } else if (stage==INITSTAGE_APPLICATION_LAYER) {
        //Schedule here to make sure the VT has computed before the metrics (since it has scheduled updates in stage 0)
        if (synchronous) {
                    simtime_t in=simTime().trunc(SIMTIME_S) + vTxRateCntrlInt;

                    scheduleAt(in, vTxRateCntrlIntTimer);
                } else {
                    scheduleAt(simTime()+uniform(0,vTxRateCntrlInt), vTxRateCntrlIntTimer);
                }
    //Have to measure the CBT when we perform the control
        if (cbfSampleTimer->isScheduled()) {
                            cancelEvent(cbfSampleTimer);
                    }
        lastPositionSent=mob->getCurrentPosition();
        emit(powerSignal,currentPower);
    }
}

void SAEJ2945::handleMessage(cMessage *msg)
{
    if (msg->isSelfMessage()) {
        if (msg==vTxRateCntrlIntTimer) {
            performControlActions();
            scheduleAt(simTime()+vTxRateCntrlInt, vTxRateCntrlIntTimer);
        } else if (msg->getKind()==CAM_GENERATION_TO) {

            sendCAMAtPower(beaconRate,currentPower);

            if (randomizedBeacon) {
                scheduleAt(simTime()+uniform(-vTxRand,vTxRand)+(beaconTime),msg);
            } else {
                scheduleAt(simTime()+(beaconTime),msg);
            }
        } else {
            CAMGenerator::handleMessage(msg);
        }

    } else {
        CAMGenerator::handleMessage(msg);
    }
}
void SAEJ2945::sendCAMAtPower(double br, double p) {
    computePDR();
    auto data = makeShared<CAMPacket>();
    data->setSource(myId);
    data->setCreationTime(simTime());
    data->setSequenceNumber(CAMsent);
    data->setChunkLength(B(beaconSize));
    data->setPosition(mob->getCurrentPosition());
    data->setSpeed(mob->getCurrentVelocity());
    data->setAngle(0.0);
    data->setBeaconRate(beaconRate);

    data->setPower(p);

    Packet* cam = new Packet("cam", data);

    //Indicate required power to physical layer
    cam->addTagIfAbsent<SignalPowerReq>()->setPower(W(p));

    sendDown(cam);
    CAMsent++;
    double cqi = check_and_cast<VehicleTableSAE*>(vehicleTable)->getCQI();
    //std::cout<<simTime()<<":"<<myId<<"cqi="<<cqi<<std::endl;
    if (!std::isnan(cqi)) {


        if (uniform(0,1)<cqi)
        {
            fails++;
            if (fails>vMaxSuccessiveFail) {
                fails=0;
                lastPositionSent=mob->getCurrentPosition();
                lastSpeedSent=mob->getCurrentVelocity();
            }
        } else {
            lastPositionSent=mob->getCurrentPosition();
            lastSpeedSent=mob->getCurrentVelocity();
        }
    }
    lastSentBeaconTime=simTime();


}
void SAEJ2945::performControlActions() {


    double e=computeTrackingError();
    double p=computeTransmissionProbablity(e);
    double myRate=computeBeaconRate();
    //std::cout<<simTime()<<":"<<myId<<"; e="<<e<<"p="<<p<<"rate="<<myRate<<std::endl;
    currentPower=computeTransmitPower();
    emit(powerSignal,currentPower);

    setBeaconRate(myRate);

    //Transmit now?
    simtime_t nextTime=beaconGenerationTimer->getArrivalTime();

    if (uniform(0,1)<=p && (nextTime-simTime()>=vRescheduleTh)) {
        if (beaconGenerationTimer->isScheduled()) {
            cancelEvent(beaconGenerationTimer);

        }

        sendCAMAtPower(myRate, vRPMax);
        scheduleAt(simTime()+beaconTime,beaconGenerationTimer);

    } else {
        if (beaconGenerationTimer->isScheduled()) {
                rescheduleBeacon(myRate);
            }
    }


}
double SAEJ2945::computeTransmitPower() {
    double txp=0.0;
    double rawCBP=getMeasuredCBT(vTxRateCntrlInt);
    double smoothCBP=rawCBP*vCBPWeightFactor + (1.0-vCBPWeightFactor)*lastCBT;
    lastCBT=rawCBP;
    if (smoothCBP <=vMinChanUtil) {
        txp=vRPMax;
    } else if (smoothCBP > vMinChanUtil && smoothCBP < vMaxChanUtil ) {
        txp = vRPMax-(((vRPMax-vRPMin)/(vMaxChanUtil-vMinChanUtil))*(smoothCBP-vMinChanUtil));
    } else if (smoothCBP >=vMaxChanUtil) {
        txp=vRPMin;
    }
    //std::cout<<simTime()<<":"<<myId<<"rawCBP="<<rawCBP<<"smoothCBP="<<smoothCBP<<"val="<<(((vRPMax-vRPMin)/(vMaxChanUtil-vMinChanUtil))*(smoothCBP-vMinChanUtil))<<"txp="<<txp<<std::endl;
    double rp=lastPower +vSUPRAGain*(txp-lastPower);
    lastPower=rp;
    if (rp>vRPMax) {
        rp=vRPMax;
    }
    return rp;
}
double SAEJ2945::computeTrackingError() {
    double e=0.0;
    //No need to estimate our current position, it is always known in our case. If GPS updates are used this should be estimated
    Coord currentPos=mob->getCurrentPosition();
    Coord remoteEstimatedPosition=lastPositionSent+lastSpeedSent*(simTime()-lastSentBeaconTime).dbl();
    e=currentPos.distance(remoteEstimatedPosition);

    return e;

}
double SAEJ2945::computeTransmissionProbablity(double e) {
    double p=0.0;
    if (e>=vTrackingErrMin && e<vTrackingErrMax) {
        double dif=e-vTrackingErrMin;
        p=1.0-exp(-vErrSensitivity*(dif*dif));
    } else if (e>=vTrackingErrMax) {
        p=1.0;
    } else {
        p=0.0;
    }

    return p;

}
double SAEJ2945::computeBeaconRate() {
    unsigned int dens=check_and_cast<VehicleTableSAE*>(vehicleTable)->getVDR();
    double smoothDens=dens*vDensityWeightFactor +lastDensity*(1.0-vDensityWeightFactor);
    lastDensity=dens;
    double max_itt=0.0;
    double th=(vmax_itt/vmin_itt)*vDensityCoefficient;

    if (smoothDens <= vDensityCoefficient) {
        max_itt=vmin_itt;
    } else if (smoothDens >vDensityCoefficient && smoothDens <th) {
        max_itt = vmin_itt *(smoothDens/vDensityCoefficient);
    } else if (smoothDens >= th) {
        max_itt=vmax_itt;
    }
    //std::cout<<simTime()<<":"<<myId<<"dens="<<dens<<"smoothDens="<<smoothDens<<"th="<<th<<std::endl;

    return (1.0/max_itt);

}
SAEJ2945::SAEJ2945() : vTxRateCntrlIntTimer(nullptr){
}

SAEJ2945::~SAEJ2945() {
    cancelAndDelete(vTxRateCntrlIntTimer);
}


} //namespace
