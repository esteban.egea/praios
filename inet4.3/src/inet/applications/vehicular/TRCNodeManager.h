//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET4_3_TRCNODEMANAGER_H_
#define __INET4_3_TRCNODEMANAGER_H_

#include <omnetpp.h>
#include "inet/common/InitStages.h"
using namespace omnetpp;

namespace inet {

/**
 * TODO - Generated class
 */
class TRCNodeManager : public cSimpleModule
{
public:
    virtual int getNumberOfNodes();
protected:
    virtual int numInitStages() const override {return NUM_INIT_STAGES;}
    virtual void initialize(int stage) override;

    int nodeVectorIndex;
    int dim;
    bool destination;
    double maxX;
    double maxY;
    std::string moduleType; /**< module type to be used in the simulation for each managed vehicle */
    std::string moduleName; /**< module name to be used in the simulation for each managed vehicle */
    std::string moduleDisplayString; /**< module displayString to be used in the simulation for each managed vehicle */

};

} //namespace

#endif
