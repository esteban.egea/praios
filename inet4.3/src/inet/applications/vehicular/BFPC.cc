//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "BFPC.h"
#include "inet/physicallayer/wireless/common/contract/packetlevel/SignalTag_m.h"
#include "CAMPacket_m.h"
#define BFPC_TXRATE_TO 101
namespace inet {

Define_Module(BFPC);
simsignal_t BFPC::powerSignal=SIMSIGNAL_NULL;

void BFPC::initialize(int stage) {
    CAMGenerator::initialize(stage);
    if (stage==INITSTAGE_LOCAL) {
        synchronous= par("synchronous");
        u=par("u");
        w=par("w");
        c=par("c");
        controlPeriod=par("controlPeriod");
        const char *vstr = par("powers").stringValue();
        powerRange= cStringTokenizer(vstr).asDoubleVector();
        maxPower=powerRange[powerRange.size()-1];
        minPower=powerRange[0];

        const char *vstr2 = par("rates").stringValue();
        beconRateRange = cStringTokenizer(vstr2).asDoubleVector();
        beaconRate= beconRateRange[beconRateRange.size()-1];
        maxRate=beaconRate;
        minRate=beconRateRange[0];

        beaconTime=1.0/beaconRate;
        controlTimer = new cMessage("Tx control timer", BFPC_TXRATE_TO );
        currentPower = par("initialPower");
        powerSignal=registerSignal("power");
    }else if (stage==INITSTAGE_APPLICATION_LAYER) {

        if (synchronous) {
            simtime_t in=simTime().trunc(SIMTIME_S) + controlPeriod;

            scheduleAt(in, controlTimer);
        } else {
            scheduleAt(simTime()+uniform(0,controlPeriod), controlTimer);
        }
        //Have to measure the CBT when we perform the control
        if (cbfSampleTimer->isScheduled()) {
            cancelEvent(cbfSampleTimer);
        }
        // TODO: THIS STRONGLY DEPENDS ON THE DATARATE, SHOULD BE ASKED TO THE PHY LAYER...But is not easy with the current API...
        //The basic encapsulation is
        // PHY Header (5) | MAC HEADER (24) | LLCEDP (2) | CAM | MAC TRAILER (4) | PHY TRAILER (2)
        //But PHY Trailer depends on the length of the CAM via the padding...Also PHY Header duration depends on the datarate
        camtime=752e-6;

        emit(powerSignal,currentPower);
    }
}

void BFPC::handleMessage(cMessage *msg) {

    if (msg->isSelfMessage()) {
        if (msg==controlTimer) {
            performControlActions();
            scheduleAt(simTime()+controlPeriod, controlTimer);
        } else if (msg->getKind()==CAM_GENERATION_TO) {

            sendCAMAtPower(beaconRate,currentPower);
            scheduleAt(simTime()+(beaconTime),msg);

        } else {
            CAMGenerator::handleMessage(msg);
        }

    } else {
        CAMGenerator::handleMessage(msg);
    }
}

void BFPC::sendCAMAtPower(double br, double p) {
    computePDR();
    auto data = makeShared<CAMPacket>();
    data->setSource(myId);
    data->setCreationTime(simTime());
    data->setSequenceNumber(CAMsent);
    data->setChunkLength(B(beaconSize));
    data->setPosition(mob->getCurrentPosition());
    data->setSpeed(mob->getCurrentVelocity());
    data->setAngle(0.0);
    data->setBeaconRate(beaconRate);

    data->setPower(p);

    Packet* cam = new Packet("cam", data);

    //Indicate required power to physical layer
    cam->addTagIfAbsent<SignalPowerReq>()->setPower(W(p));

    sendDown(cam);
    CAMsent++;
}

void BFPC::performControlActions() {
    double cbr=getMeasuredCBT(controlPeriod);
    double ucbr=(1-cbr);
    //In the BFPC it is not said, but it seem that the power has to be in mW for this to work, or either
    //the typical value of parameters has to be divided by 1000
    double p_mW=currentPower*1000;
    p_mW=p_mW + (w/(p_mW+1)) - (c/ucbr);
    currentPower=p_mW/1000.0;
    if (currentPower>maxPower) {
        currentPower=maxPower;
        p_mW =maxPower*1000.0;
    }else if (currentPower<minPower) {
        currentPower=minPower;
        p_mW =minPower*1000.0;
    }
    beaconRate=beaconRate + (u/(beaconRate+1)) - (c*p_mW*camtime/(ucbr*ucbr));

    if (beaconRate>maxRate) {
        beaconRate=maxRate;
    }else if (beaconRate<minRate) {
        beaconRate=minRate;
    }

    emit(powerSignal,currentPower);
    if (beaconGenerationTimer->isScheduled()) {
        rescheduleBeacon(beaconRate);
    }
    setBeaconRate(beaconRate);
    //std::cout<<myId<<":"<<simTime()<<":"<<cbr<<"ucbr="<<ucbr<<"(w/(p_mW+1))="<<(w/(p_mW+1))<<"(c/ucbr)="<<(c/ucbr)<<"(u/(beaconRate+1))"<<(u/(beaconRate+1))<<"(c*p_mW*camtime/(ucbr*ucbr))="<<(c*p_mW*camtime/(ucbr*ucbr))<<std::endl;
    //std::cout<<myId<<":"<<simTime()<<":p="<<currentPower<<"r="<<beaconRate<<std::endl;
}



} //namespace
