//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET4_3_SAEJ2945_H_
#define __INET4_3_SAEJ2945_H_

#include <omnetpp.h>
#include "CAMGenerator.h"

#include "inet/physicallayer/wireless/common/contract/packetlevel/IRadio.h"
using namespace omnetpp;

namespace inet {

/**
 * TODO - Generated class
 */
class SAEJ2945 : public CAMGenerator
{
public:
    SAEJ2945();
    virtual ~SAEJ2945();
  protected:
    virtual void initialize(int stage) override;
    virtual void handleMessage(cMessage *msg) override;
    virtual void sendCAMAtPower(double br, double p);
    virtual void performControlActions();
    virtual double computeTrackingError();
    virtual double computeTransmissionProbablity(double e);
    virtual double computeBeaconRate();
    virtual double computeTransmitPower();
    std::vector<double> beconRateRange;
    std::vector<double> powerRange;

    double vTxRateCntrlInt;
    cMessage* vTxRateCntrlIntTimer;
    bool synchronous;
    bool randomizedBeacon;
    double currentPower;
    double vTrackingErrMin;
    double vTrackingErrMax;
    double vErrSensitivity;
    double vDensityCoefficient;
    double vDensityWeightFactor;
    double vRescheduleTh;
    double vCBPWeightFactor;
    double vSUPRAGain;
    double vTxRand;
    int vMaxSuccessiveFail;
    int fails;
    simtime_t lastSentBeaconTime;
    Coord lastPositionSent;
    Coord lastSpeedSent;
    double lastDensity;
    double lastCBT;
    double lastPower;
    double vmin_itt;
    double vmax_itt;
    double vMinChanUtil;
    double vMaxChanUtil;
    //Max and min powers
    double vRPMax;
    double vRPMin;


    static simsignal_t powerSignal;
};

} //namespace

#endif
