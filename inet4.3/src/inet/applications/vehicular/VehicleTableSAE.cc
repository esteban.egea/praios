//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "VehicleTableSAE.h"

namespace inet {
#define UPDATE_vPERIntervalTimer_TO 4002
Define_Module(VehicleTableSAE);

VehicleTableSAE::~VehicleTableSAE() {
    cancelAndDelete(vPERIntervalTimer);

}

void VehicleTableSAE::initialize()
{
    VehicleTable::initialize();
    vPERRange=par("vPERRange");
    vPERInterval=par("vPERInterval");
    vPERSubInterval=par("vPERSubInterval");
    vPERMax=par("vPERMax");
    vPERIntervalTimer = new cMessage("vPERIntervalTimer update", UPDATE_vPERIntervalTimer_TO );
    scheduleAt(simTime()+vPERInterval, vPERIntervalTimer);
    cancelUpdateTimer();
    scheduleAt(simTime()+vPERSubInterval, updateTimer);

    currentSubInterval=0;
    currentCQI=NaN;
    currentVDR=0;
    rsquared=vPERRange*vPERRange;
    WATCH(currentCQI);
    WATCH(currentVDR);
    WATCH(currentSubInterval);
}
double VehicleTableSAE::getCQI() const {
    return currentCQI;
}
double VehicleTableSAE::getVDR() const {
    return currentVDR;
}
unsigned int VehicleTableSAE::computeVehicleDensityInRange() {
    //Returns numbers of vehicles at less than vPERRange
    VTableSAE::iterator  it=vtsae.begin();

    unsigned int n=0;
    while(it!=vtsae.end()) {
        auto vehicle=it->second[currentSubInterval];
        double distance = mob->getCurrentPosition().sqrdist(vehicle->pos);
        //std::cout<<"neip="<<"distance="<<distance<<"rsquared="<<rsquared<<"pos="<<vehicle->pos<<std::endl;
        if (distance<=rsquared && vehicle->beaconsReceived>0) {
            ++n;
        }
        ++it;


    }
    //std::cout<<"nei="<<vtsae.size()<<"n="<<n<<"currentSubInterval="<<currentSubInterval<<std::endl;
    return n;
}

double VehicleTableSAE::computeChannelQualityIndicator() {
    double per=0.0;
    unsigned int samples=0;
    VTableSAE::iterator  it=vtsae.begin();

    while(it!=vtsae.end()) {
        auto infos=it->second;
        int rx=0;
        int e=0;
        for (unsigned int i=0;i<vPERInterval;i++) {
            rx += infos[i]->beaconsReceived;
            e +=floor(infos[i]->beaconRate);
            //std::cout<<simTime()<<":"<<i<<"e="<<e<<"rx="<<rx<<std::endl;
        }
        if (e>0) {
            auto vehicle=infos[currentSubInterval];
            //Only vehicles within vPERRange are included in CQI
            double distance = mob->getCurrentPosition().sqrdist(vehicle->pos);
            //std::cout<<"neip="<<"distance="<<distance<<"rsquared="<<rsquared<<"pos="<<vehicle->pos<<std::endl;
            if (distance<=rsquared && vehicle->beaconsReceived>0) {
                ++samples;
                int missing=e-rx;
                //This may happen because of the transmissions due to dynamic conditions (prob. of trans due to tracking error)
                if (missing<0) {
                    missing=0;
                }

                per += ((double)missing)/e;
                //std::cout<<simTime()<<"samples="<<samples<<"per="<<per<<std::endl;
            }

        }
        ++it;
    }
    if (samples>0) {
        per=per/samples;
        //std::cout<<simTime()<<"samples="<<samples<<"avper="<<per<<std::endl;
        if (per>vPERMax) {
            return vPERMax;
        } else {
            return per;
        }
    } else {
        return NaN;
    }
}

void VehicleTableSAE::handleMessage(cMessage *msg)
{
    if (msg->isSelfMessage()) {
        if (msg==updateTimer) {
            refreshTableVPERSubInterval();
            scheduleAt(simTime()+vPERSubInterval, updateTimer);
        }
        if (msg==vPERIntervalTimer) {
            refreshTableVPERInterval();
            scheduleAt(simTime()+vPERInterval, vPERIntervalTimer);
        }
        //emit(neighbors, 0);
    }
}
int  VehicleTableSAE::insertOrUpdate(VehicleInfo* info) {

    emit(neighbors,vtsae.size());
    VTableSAE::iterator it = vtsae.find(info->id);
    if (it==vtsae.end()) {
        VehicleInfo* newNeigbor=new VehicleInfo(info->id,info->pos,info->speed,info->beaconRate,info->weight,info->power);
        newNeigbor->beaconsReceived++;
        std::vector<VehicleInfo*> infos(vPERInterval);
        for (unsigned int i=0; i<vPERInterval; i++) {
            if (i==currentSubInterval) {
                infos[i]=newNeigbor;
            } else {
                infos[i]=new VehicleInfo(info->id,info->pos,info->speed,0,info->weight,info->power);
            }

        }
        vtsae.emplace(info->id,infos);

        return 1;
    } else {
        auto vehicle=it->second[currentSubInterval];
        double irt_time=(simTime()-vehicle->last_update).dbl();
        double distance = mob->getCurrentPosition().distance(info->pos);
        irthist->collect(irt_time,distance);
        emit(irt,irt_time);
        emit(rrt,1/(irt_time*vehicle->beaconRate));
        int brp=vehicle->beaconsReceived;
        double mbrp=vehicle->measuredBeaconRate;
        double prevBr=vehicle->beaconRate;
        (*vehicle) = *info;
        vehicle->last_update=simTime();
        vehicle->beaconsReceived=brp+1;
        //TODO:The expected number of beacons is needed to compute the CQI. It may change in the middle of
        //an interval. I just average over previous beacon rates right now...
        vehicle->beaconRate=(prevBr+info->beaconRate)/2.0;

        vehicle->measuredBeaconRate=mbrp;
        /*auto infos=it->second;
        for (unsigned int i=0; i<vPERInterval; i++) {
            std::cout<<simTime()<<"update; "<<i<<"; beaconsReceived="<<infos[i]->beaconsReceived<<std::endl;
        }
         */
        return 0;
    }

}
void VehicleTableSAE::refreshTableVPERSubInterval(){
    currentCQI=computeChannelQualityIndicator();
    currentVDR=computeVehicleDensityInRange();
    //std::cout<<simTime()<<"cqi="<<currentCQI<<"vdr="<<currentVDR<<std::endl;
    currentSubInterval++;
    if (currentSubInterval==vPERInterval) {
        currentSubInterval=0;
    }
}
void VehicleTableSAE::refreshTableVPERInterval(){
    //Delete old neighbors
    emit(neighbors,vtsae.size());
    VTableSAE::iterator  it=vtsae.begin();


    while(it!=vtsae.end()) {

        //Delete if we have not received a beacon in the last interval
        if (it->second[currentSubInterval]->beaconsReceived==0) {

            for (unsigned int i=0; i<vPERInterval; i++) {
                delete(it->second[i]);
            }
            it=vtsae.erase(it);
        } else {
            it->second[currentSubInterval]->beaconsReceived=0;
            ++it;
        }

    }
    currentSubInterval=0;

}
} //namespace
