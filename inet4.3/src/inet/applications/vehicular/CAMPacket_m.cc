//
// Generated file, do not edit! Created by opp_msgtool 6.0 from inet/applications/vehicular/CAMPacket.msg.
//

// Disable warnings about unused variables, empty switch stmts, etc:
#ifdef _MSC_VER
#  pragma warning(disable:4101)
#  pragma warning(disable:4065)
#endif

#if defined(__clang__)
#  pragma clang diagnostic ignored "-Wshadow"
#  pragma clang diagnostic ignored "-Wconversion"
#  pragma clang diagnostic ignored "-Wunused-parameter"
#  pragma clang diagnostic ignored "-Wc++98-compat"
#  pragma clang diagnostic ignored "-Wunreachable-code-break"
#  pragma clang diagnostic ignored "-Wold-style-cast"
#elif defined(__GNUC__)
#  pragma GCC diagnostic ignored "-Wshadow"
#  pragma GCC diagnostic ignored "-Wconversion"
#  pragma GCC diagnostic ignored "-Wunused-parameter"
#  pragma GCC diagnostic ignored "-Wold-style-cast"
#  pragma GCC diagnostic ignored "-Wsuggest-attribute=noreturn"
#  pragma GCC diagnostic ignored "-Wfloat-conversion"
#endif

#include <iostream>
#include <sstream>
#include <memory>
#include <type_traits>
#include "CAMPacket_m.h"

namespace omnetpp {

// Template pack/unpack rules. They are declared *after* a1l type-specific pack functions for multiple reasons.
// They are in the omnetpp namespace, to allow them to be found by argument-dependent lookup via the cCommBuffer argument

// Packing/unpacking an std::vector
template<typename T, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::vector<T,A>& v)
{
    int n = v.size();
    doParsimPacking(buffer, n);
    for (int i = 0; i < n; i++)
        doParsimPacking(buffer, v[i]);
}

template<typename T, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::vector<T,A>& v)
{
    int n;
    doParsimUnpacking(buffer, n);
    v.resize(n);
    for (int i = 0; i < n; i++)
        doParsimUnpacking(buffer, v[i]);
}

// Packing/unpacking an std::list
template<typename T, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::list<T,A>& l)
{
    doParsimPacking(buffer, (int)l.size());
    for (typename std::list<T,A>::const_iterator it = l.begin(); it != l.end(); ++it)
        doParsimPacking(buffer, (T&)*it);
}

template<typename T, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::list<T,A>& l)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i = 0; i < n; i++) {
        l.push_back(T());
        doParsimUnpacking(buffer, l.back());
    }
}

// Packing/unpacking an std::set
template<typename T, typename Tr, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::set<T,Tr,A>& s)
{
    doParsimPacking(buffer, (int)s.size());
    for (typename std::set<T,Tr,A>::const_iterator it = s.begin(); it != s.end(); ++it)
        doParsimPacking(buffer, *it);
}

template<typename T, typename Tr, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::set<T,Tr,A>& s)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i = 0; i < n; i++) {
        T x;
        doParsimUnpacking(buffer, x);
        s.insert(x);
    }
}

// Packing/unpacking an std::map
template<typename K, typename V, typename Tr, typename A>
void doParsimPacking(omnetpp::cCommBuffer *buffer, const std::map<K,V,Tr,A>& m)
{
    doParsimPacking(buffer, (int)m.size());
    for (typename std::map<K,V,Tr,A>::const_iterator it = m.begin(); it != m.end(); ++it) {
        doParsimPacking(buffer, it->first);
        doParsimPacking(buffer, it->second);
    }
}

template<typename K, typename V, typename Tr, typename A>
void doParsimUnpacking(omnetpp::cCommBuffer *buffer, std::map<K,V,Tr,A>& m)
{
    int n;
    doParsimUnpacking(buffer, n);
    for (int i = 0; i < n; i++) {
        K k; V v;
        doParsimUnpacking(buffer, k);
        doParsimUnpacking(buffer, v);
        m[k] = v;
    }
}

// Default pack/unpack function for arrays
template<typename T>
void doParsimArrayPacking(omnetpp::cCommBuffer *b, const T *t, int n)
{
    for (int i = 0; i < n; i++)
        doParsimPacking(b, t[i]);
}

template<typename T>
void doParsimArrayUnpacking(omnetpp::cCommBuffer *b, T *t, int n)
{
    for (int i = 0; i < n; i++)
        doParsimUnpacking(b, t[i]);
}

// Default rule to prevent compiler from choosing base class' doParsimPacking() function
template<typename T>
void doParsimPacking(omnetpp::cCommBuffer *, const T& t)
{
    throw omnetpp::cRuntimeError("Parsim error: No doParsimPacking() function for type %s", omnetpp::opp_typename(typeid(t)));
}

template<typename T>
void doParsimUnpacking(omnetpp::cCommBuffer *, T& t)
{
    throw omnetpp::cRuntimeError("Parsim error: No doParsimUnpacking() function for type %s", omnetpp::opp_typename(typeid(t)));
}

}  // namespace omnetpp

class inet__CoordDescriptor : public omnetpp::cClassDescriptor
{
  private:
    mutable const char **propertyNames;
    enum FieldConstants {
    };
  public:
    inet__CoordDescriptor();
    virtual ~inet__CoordDescriptor();

    virtual bool doesSupport(omnetpp::cObject *obj) const override;
    virtual const char **getPropertyNames() const override;
    virtual const char *getProperty(const char *propertyName) const override;
    virtual int getFieldCount() const override;
    virtual const char *getFieldName(int field) const override;
    virtual int findField(const char *fieldName) const override;
    virtual unsigned int getFieldTypeFlags(int field) const override;
    virtual const char *getFieldTypeString(int field) const override;
    virtual const char **getFieldPropertyNames(int field) const override;
    virtual const char *getFieldProperty(int field, const char *propertyName) const override;
    virtual int getFieldArraySize(omnetpp::any_ptr object, int field) const override;
    virtual void setFieldArraySize(omnetpp::any_ptr object, int field, int size) const override;

    virtual const char *getFieldDynamicTypeString(omnetpp::any_ptr object, int field, int i) const override;
    virtual std::string getFieldValueAsString(omnetpp::any_ptr object, int field, int i) const override;
    virtual void setFieldValueAsString(omnetpp::any_ptr object, int field, int i, const char *value) const override;
    virtual omnetpp::cValue getFieldValue(omnetpp::any_ptr object, int field, int i) const override;
    virtual void setFieldValue(omnetpp::any_ptr object, int field, int i, const omnetpp::cValue& value) const override;

    virtual const char *getFieldStructName(int field) const override;
    virtual omnetpp::any_ptr getFieldStructValuePointer(omnetpp::any_ptr object, int field, int i) const override;
    virtual void setFieldStructValuePointer(omnetpp::any_ptr object, int field, int i, omnetpp::any_ptr ptr) const override;
};

Register_ClassDescriptor(inet__CoordDescriptor)

inet__CoordDescriptor::inet__CoordDescriptor() : omnetpp::cClassDescriptor(omnetpp::opp_typename(typeid(inet::Coord)), "")
{
    propertyNames = nullptr;
}

inet__CoordDescriptor::~inet__CoordDescriptor()
{
    delete[] propertyNames;
}

bool inet__CoordDescriptor::doesSupport(omnetpp::cObject *obj) const
{
    return dynamic_cast<inet::Coord *>(obj)!=nullptr;
}

const char **inet__CoordDescriptor::getPropertyNames() const
{
    if (!propertyNames) {
        static const char *names[] = { "existingClass",  nullptr };
        omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
        const char **baseNames = base ? base->getPropertyNames() : nullptr;
        propertyNames = mergeLists(baseNames, names);
    }
    return propertyNames;
}

const char *inet__CoordDescriptor::getProperty(const char *propertyName) const
{
    if (!strcmp(propertyName, "existingClass")) return "";
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    return base ? base->getProperty(propertyName) : nullptr;
}

int inet__CoordDescriptor::getFieldCount() const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    return base ? 0+base->getFieldCount() : 0;
}

unsigned int inet__CoordDescriptor::getFieldTypeFlags(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldTypeFlags(field);
        field -= base->getFieldCount();
    }
    return 0;
}

const char *inet__CoordDescriptor::getFieldName(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldName(field);
        field -= base->getFieldCount();
    }
    return nullptr;
}

int inet__CoordDescriptor::findField(const char *fieldName) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    return base ? base->findField(fieldName) : -1;
}

const char *inet__CoordDescriptor::getFieldTypeString(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldTypeString(field);
        field -= base->getFieldCount();
    }
    return nullptr;
}

const char **inet__CoordDescriptor::getFieldPropertyNames(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldPropertyNames(field);
        field -= base->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

const char *inet__CoordDescriptor::getFieldProperty(int field, const char *propertyName) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldProperty(field, propertyName);
        field -= base->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

int inet__CoordDescriptor::getFieldArraySize(omnetpp::any_ptr object, int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldArraySize(object, field);
        field -= base->getFieldCount();
    }
    inet::Coord *pp = omnetpp::fromAnyPtr<inet::Coord>(object); (void)pp;
    switch (field) {
        default: return 0;
    }
}

void inet__CoordDescriptor::setFieldArraySize(omnetpp::any_ptr object, int field, int size) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldArraySize(object, field, size);
            return;
        }
        field -= base->getFieldCount();
    }
    inet::Coord *pp = omnetpp::fromAnyPtr<inet::Coord>(object); (void)pp;
    switch (field) {
        default: throw omnetpp::cRuntimeError("Cannot set array size of field %d of class 'inet::Coord'", field);
    }
}

const char *inet__CoordDescriptor::getFieldDynamicTypeString(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldDynamicTypeString(object,field,i);
        field -= base->getFieldCount();
    }
    inet::Coord *pp = omnetpp::fromAnyPtr<inet::Coord>(object); (void)pp;
    switch (field) {
        default: return nullptr;
    }
}

std::string inet__CoordDescriptor::getFieldValueAsString(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldValueAsString(object,field,i);
        field -= base->getFieldCount();
    }
    inet::Coord *pp = omnetpp::fromAnyPtr<inet::Coord>(object); (void)pp;
    switch (field) {
        default: return "";
    }
}

void inet__CoordDescriptor::setFieldValueAsString(omnetpp::any_ptr object, int field, int i, const char *value) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldValueAsString(object, field, i, value);
            return;
        }
        field -= base->getFieldCount();
    }
    inet::Coord *pp = omnetpp::fromAnyPtr<inet::Coord>(object); (void)pp;
    switch (field) {
        default: throw omnetpp::cRuntimeError("Cannot set field %d of class 'inet::Coord'", field);
    }
}

omnetpp::cValue inet__CoordDescriptor::getFieldValue(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldValue(object,field,i);
        field -= base->getFieldCount();
    }
    inet::Coord *pp = omnetpp::fromAnyPtr<inet::Coord>(object); (void)pp;
    switch (field) {
        default: throw omnetpp::cRuntimeError("Cannot return field %d of class 'inet::Coord' as cValue -- field index out of range?", field);
    }
}

void inet__CoordDescriptor::setFieldValue(omnetpp::any_ptr object, int field, int i, const omnetpp::cValue& value) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldValue(object, field, i, value);
            return;
        }
        field -= base->getFieldCount();
    }
    inet::Coord *pp = omnetpp::fromAnyPtr<inet::Coord>(object); (void)pp;
    switch (field) {
        default: throw omnetpp::cRuntimeError("Cannot set field %d of class 'inet::Coord'", field);
    }
}

const char *inet__CoordDescriptor::getFieldStructName(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldStructName(field);
        field -= base->getFieldCount();
    }
    return nullptr;
}

omnetpp::any_ptr inet__CoordDescriptor::getFieldStructValuePointer(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldStructValuePointer(object, field, i);
        field -= base->getFieldCount();
    }
    inet::Coord *pp = omnetpp::fromAnyPtr<inet::Coord>(object); (void)pp;
    switch (field) {
        default: return omnetpp::any_ptr(nullptr);
    }
}

void inet__CoordDescriptor::setFieldStructValuePointer(omnetpp::any_ptr object, int field, int i, omnetpp::any_ptr ptr) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldStructValuePointer(object, field, i, ptr);
            return;
        }
        field -= base->getFieldCount();
    }
    inet::Coord *pp = omnetpp::fromAnyPtr<inet::Coord>(object); (void)pp;
    switch (field) {
        default: throw omnetpp::cRuntimeError("Cannot set field %d of class 'inet::Coord'", field);
    }
}

Register_Class(SequenceNumberTag)

SequenceNumberTag::SequenceNumberTag() : ::inet::TagBase()
{
}

SequenceNumberTag::SequenceNumberTag(const SequenceNumberTag& other) : ::inet::TagBase(other)
{
    copy(other);
}

SequenceNumberTag::~SequenceNumberTag()
{
}

SequenceNumberTag& SequenceNumberTag::operator=(const SequenceNumberTag& other)
{
    if (this == &other) return *this;
    ::inet::TagBase::operator=(other);
    copy(other);
    return *this;
}

void SequenceNumberTag::copy(const SequenceNumberTag& other)
{
    this->sequenceNumber = other.sequenceNumber;
}

void SequenceNumberTag::parsimPack(omnetpp::cCommBuffer *b) const
{
    ::inet::TagBase::parsimPack(b);
    doParsimPacking(b,this->sequenceNumber);
}

void SequenceNumberTag::parsimUnpack(omnetpp::cCommBuffer *b)
{
    ::inet::TagBase::parsimUnpack(b);
    doParsimUnpacking(b,this->sequenceNumber);
}

int SequenceNumberTag::getSequenceNumber() const
{
    return this->sequenceNumber;
}

void SequenceNumberTag::setSequenceNumber(int sequenceNumber)
{
    this->sequenceNumber = sequenceNumber;
}

class SequenceNumberTagDescriptor : public omnetpp::cClassDescriptor
{
  private:
    mutable const char **propertyNames;
    enum FieldConstants {
        FIELD_sequenceNumber,
    };
  public:
    SequenceNumberTagDescriptor();
    virtual ~SequenceNumberTagDescriptor();

    virtual bool doesSupport(omnetpp::cObject *obj) const override;
    virtual const char **getPropertyNames() const override;
    virtual const char *getProperty(const char *propertyName) const override;
    virtual int getFieldCount() const override;
    virtual const char *getFieldName(int field) const override;
    virtual int findField(const char *fieldName) const override;
    virtual unsigned int getFieldTypeFlags(int field) const override;
    virtual const char *getFieldTypeString(int field) const override;
    virtual const char **getFieldPropertyNames(int field) const override;
    virtual const char *getFieldProperty(int field, const char *propertyName) const override;
    virtual int getFieldArraySize(omnetpp::any_ptr object, int field) const override;
    virtual void setFieldArraySize(omnetpp::any_ptr object, int field, int size) const override;

    virtual const char *getFieldDynamicTypeString(omnetpp::any_ptr object, int field, int i) const override;
    virtual std::string getFieldValueAsString(omnetpp::any_ptr object, int field, int i) const override;
    virtual void setFieldValueAsString(omnetpp::any_ptr object, int field, int i, const char *value) const override;
    virtual omnetpp::cValue getFieldValue(omnetpp::any_ptr object, int field, int i) const override;
    virtual void setFieldValue(omnetpp::any_ptr object, int field, int i, const omnetpp::cValue& value) const override;

    virtual const char *getFieldStructName(int field) const override;
    virtual omnetpp::any_ptr getFieldStructValuePointer(omnetpp::any_ptr object, int field, int i) const override;
    virtual void setFieldStructValuePointer(omnetpp::any_ptr object, int field, int i, omnetpp::any_ptr ptr) const override;
};

Register_ClassDescriptor(SequenceNumberTagDescriptor)

SequenceNumberTagDescriptor::SequenceNumberTagDescriptor() : omnetpp::cClassDescriptor(omnetpp::opp_typename(typeid(SequenceNumberTag)), "inet::TagBase")
{
    propertyNames = nullptr;
}

SequenceNumberTagDescriptor::~SequenceNumberTagDescriptor()
{
    delete[] propertyNames;
}

bool SequenceNumberTagDescriptor::doesSupport(omnetpp::cObject *obj) const
{
    return dynamic_cast<SequenceNumberTag *>(obj)!=nullptr;
}

const char **SequenceNumberTagDescriptor::getPropertyNames() const
{
    if (!propertyNames) {
        static const char *names[] = {  nullptr };
        omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
        const char **baseNames = base ? base->getPropertyNames() : nullptr;
        propertyNames = mergeLists(baseNames, names);
    }
    return propertyNames;
}

const char *SequenceNumberTagDescriptor::getProperty(const char *propertyName) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    return base ? base->getProperty(propertyName) : nullptr;
}

int SequenceNumberTagDescriptor::getFieldCount() const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    return base ? 1+base->getFieldCount() : 1;
}

unsigned int SequenceNumberTagDescriptor::getFieldTypeFlags(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldTypeFlags(field);
        field -= base->getFieldCount();
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISEDITABLE,    // FIELD_sequenceNumber
    };
    return (field >= 0 && field < 1) ? fieldTypeFlags[field] : 0;
}

const char *SequenceNumberTagDescriptor::getFieldName(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldName(field);
        field -= base->getFieldCount();
    }
    static const char *fieldNames[] = {
        "sequenceNumber",
    };
    return (field >= 0 && field < 1) ? fieldNames[field] : nullptr;
}

int SequenceNumberTagDescriptor::findField(const char *fieldName) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    int baseIndex = base ? base->getFieldCount() : 0;
    if (strcmp(fieldName, "sequenceNumber") == 0) return baseIndex + 0;
    return base ? base->findField(fieldName) : -1;
}

const char *SequenceNumberTagDescriptor::getFieldTypeString(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldTypeString(field);
        field -= base->getFieldCount();
    }
    static const char *fieldTypeStrings[] = {
        "int",    // FIELD_sequenceNumber
    };
    return (field >= 0 && field < 1) ? fieldTypeStrings[field] : nullptr;
}

const char **SequenceNumberTagDescriptor::getFieldPropertyNames(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldPropertyNames(field);
        field -= base->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

const char *SequenceNumberTagDescriptor::getFieldProperty(int field, const char *propertyName) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldProperty(field, propertyName);
        field -= base->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

int SequenceNumberTagDescriptor::getFieldArraySize(omnetpp::any_ptr object, int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldArraySize(object, field);
        field -= base->getFieldCount();
    }
    SequenceNumberTag *pp = omnetpp::fromAnyPtr<SequenceNumberTag>(object); (void)pp;
    switch (field) {
        default: return 0;
    }
}

void SequenceNumberTagDescriptor::setFieldArraySize(omnetpp::any_ptr object, int field, int size) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldArraySize(object, field, size);
            return;
        }
        field -= base->getFieldCount();
    }
    SequenceNumberTag *pp = omnetpp::fromAnyPtr<SequenceNumberTag>(object); (void)pp;
    switch (field) {
        default: throw omnetpp::cRuntimeError("Cannot set array size of field %d of class 'SequenceNumberTag'", field);
    }
}

const char *SequenceNumberTagDescriptor::getFieldDynamicTypeString(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldDynamicTypeString(object,field,i);
        field -= base->getFieldCount();
    }
    SequenceNumberTag *pp = omnetpp::fromAnyPtr<SequenceNumberTag>(object); (void)pp;
    switch (field) {
        default: return nullptr;
    }
}

std::string SequenceNumberTagDescriptor::getFieldValueAsString(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldValueAsString(object,field,i);
        field -= base->getFieldCount();
    }
    SequenceNumberTag *pp = omnetpp::fromAnyPtr<SequenceNumberTag>(object); (void)pp;
    switch (field) {
        case FIELD_sequenceNumber: return long2string(pp->getSequenceNumber());
        default: return "";
    }
}

void SequenceNumberTagDescriptor::setFieldValueAsString(omnetpp::any_ptr object, int field, int i, const char *value) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldValueAsString(object, field, i, value);
            return;
        }
        field -= base->getFieldCount();
    }
    SequenceNumberTag *pp = omnetpp::fromAnyPtr<SequenceNumberTag>(object); (void)pp;
    switch (field) {
        case FIELD_sequenceNumber: pp->setSequenceNumber(string2long(value)); break;
        default: throw omnetpp::cRuntimeError("Cannot set field %d of class 'SequenceNumberTag'", field);
    }
}

omnetpp::cValue SequenceNumberTagDescriptor::getFieldValue(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldValue(object,field,i);
        field -= base->getFieldCount();
    }
    SequenceNumberTag *pp = omnetpp::fromAnyPtr<SequenceNumberTag>(object); (void)pp;
    switch (field) {
        case FIELD_sequenceNumber: return pp->getSequenceNumber();
        default: throw omnetpp::cRuntimeError("Cannot return field %d of class 'SequenceNumberTag' as cValue -- field index out of range?", field);
    }
}

void SequenceNumberTagDescriptor::setFieldValue(omnetpp::any_ptr object, int field, int i, const omnetpp::cValue& value) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldValue(object, field, i, value);
            return;
        }
        field -= base->getFieldCount();
    }
    SequenceNumberTag *pp = omnetpp::fromAnyPtr<SequenceNumberTag>(object); (void)pp;
    switch (field) {
        case FIELD_sequenceNumber: pp->setSequenceNumber(omnetpp::checked_int_cast<int>(value.intValue())); break;
        default: throw omnetpp::cRuntimeError("Cannot set field %d of class 'SequenceNumberTag'", field);
    }
}

const char *SequenceNumberTagDescriptor::getFieldStructName(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldStructName(field);
        field -= base->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    };
}

omnetpp::any_ptr SequenceNumberTagDescriptor::getFieldStructValuePointer(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldStructValuePointer(object, field, i);
        field -= base->getFieldCount();
    }
    SequenceNumberTag *pp = omnetpp::fromAnyPtr<SequenceNumberTag>(object); (void)pp;
    switch (field) {
        default: return omnetpp::any_ptr(nullptr);
    }
}

void SequenceNumberTagDescriptor::setFieldStructValuePointer(omnetpp::any_ptr object, int field, int i, omnetpp::any_ptr ptr) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldStructValuePointer(object, field, i, ptr);
            return;
        }
        field -= base->getFieldCount();
    }
    SequenceNumberTag *pp = omnetpp::fromAnyPtr<SequenceNumberTag>(object); (void)pp;
    switch (field) {
        default: throw omnetpp::cRuntimeError("Cannot set field %d of class 'SequenceNumberTag'", field);
    }
}

Register_Class(CAMPacket)

CAMPacket::CAMPacket() : ::inet::FieldsChunk()
{
}

CAMPacket::CAMPacket(const CAMPacket& other) : ::inet::FieldsChunk(other)
{
    copy(other);
}

CAMPacket::~CAMPacket()
{
}

CAMPacket& CAMPacket::operator=(const CAMPacket& other)
{
    if (this == &other) return *this;
    ::inet::FieldsChunk::operator=(other);
    copy(other);
    return *this;
}

void CAMPacket::copy(const CAMPacket& other)
{
    this->speed = other.speed;
    this->beaconRate = other.beaconRate;
    this->source = other.source;
    this->sequenceNumber = other.sequenceNumber;
    this->angle = other.angle;
    this->creationTime = other.creationTime;
    this->position = other.position;
    this->weight = other.weight;
    this->CBT = other.CBT;
    this->neighCBT = other.neighCBT;
    this->power = other.power;
    this->deltaF = other.deltaF;
    this->neighCBTTimestamp = other.neighCBTTimestamp;
}

void CAMPacket::parsimPack(omnetpp::cCommBuffer *b) const
{
    ::inet::FieldsChunk::parsimPack(b);
    doParsimPacking(b,this->speed);
    doParsimPacking(b,this->beaconRate);
    doParsimPacking(b,this->source);
    doParsimPacking(b,this->sequenceNumber);
    doParsimPacking(b,this->angle);
    doParsimPacking(b,this->creationTime);
    doParsimPacking(b,this->position);
    doParsimPacking(b,this->weight);
    doParsimPacking(b,this->CBT);
    doParsimPacking(b,this->neighCBT);
    doParsimPacking(b,this->power);
    doParsimPacking(b,this->deltaF);
    doParsimPacking(b,this->neighCBTTimestamp);
}

void CAMPacket::parsimUnpack(omnetpp::cCommBuffer *b)
{
    ::inet::FieldsChunk::parsimUnpack(b);
    doParsimUnpacking(b,this->speed);
    doParsimUnpacking(b,this->beaconRate);
    doParsimUnpacking(b,this->source);
    doParsimUnpacking(b,this->sequenceNumber);
    doParsimUnpacking(b,this->angle);
    doParsimUnpacking(b,this->creationTime);
    doParsimUnpacking(b,this->position);
    doParsimUnpacking(b,this->weight);
    doParsimUnpacking(b,this->CBT);
    doParsimUnpacking(b,this->neighCBT);
    doParsimUnpacking(b,this->power);
    doParsimUnpacking(b,this->deltaF);
    doParsimUnpacking(b,this->neighCBTTimestamp);
}

const inet::Coord& CAMPacket::getSpeed() const
{
    return this->speed;
}

void CAMPacket::setSpeed(const inet::Coord& speed)
{
    handleChange();
    this->speed = speed;
}

double CAMPacket::getBeaconRate() const
{
    return this->beaconRate;
}

void CAMPacket::setBeaconRate(double beaconRate)
{
    handleChange();
    this->beaconRate = beaconRate;
}

int CAMPacket::getSource() const
{
    return this->source;
}

void CAMPacket::setSource(int source)
{
    handleChange();
    this->source = source;
}

int CAMPacket::getSequenceNumber() const
{
    return this->sequenceNumber;
}

void CAMPacket::setSequenceNumber(int sequenceNumber)
{
    handleChange();
    this->sequenceNumber = sequenceNumber;
}

double CAMPacket::getAngle() const
{
    return this->angle;
}

void CAMPacket::setAngle(double angle)
{
    handleChange();
    this->angle = angle;
}

omnetpp::simtime_t CAMPacket::getCreationTime() const
{
    return this->creationTime;
}

void CAMPacket::setCreationTime(omnetpp::simtime_t creationTime)
{
    handleChange();
    this->creationTime = creationTime;
}

const inet::Coord& CAMPacket::getPosition() const
{
    return this->position;
}

void CAMPacket::setPosition(const inet::Coord& position)
{
    handleChange();
    this->position = position;
}

double CAMPacket::getWeight() const
{
    return this->weight;
}

void CAMPacket::setWeight(double weight)
{
    handleChange();
    this->weight = weight;
}

double CAMPacket::getCBT() const
{
    return this->CBT;
}

void CAMPacket::setCBT(double CBT)
{
    handleChange();
    this->CBT = CBT;
}

double CAMPacket::getNeighCBT() const
{
    return this->neighCBT;
}

void CAMPacket::setNeighCBT(double neighCBT)
{
    handleChange();
    this->neighCBT = neighCBT;
}

double CAMPacket::getPower() const
{
    return this->power;
}

void CAMPacket::setPower(double power)
{
    handleChange();
    this->power = power;
}

double CAMPacket::getDeltaF() const
{
    return this->deltaF;
}

void CAMPacket::setDeltaF(double deltaF)
{
    handleChange();
    this->deltaF = deltaF;
}

omnetpp::simtime_t CAMPacket::getNeighCBTTimestamp() const
{
    return this->neighCBTTimestamp;
}

void CAMPacket::setNeighCBTTimestamp(omnetpp::simtime_t neighCBTTimestamp)
{
    handleChange();
    this->neighCBTTimestamp = neighCBTTimestamp;
}

class CAMPacketDescriptor : public omnetpp::cClassDescriptor
{
  private:
    mutable const char **propertyNames;
    enum FieldConstants {
        FIELD_speed,
        FIELD_beaconRate,
        FIELD_source,
        FIELD_sequenceNumber,
        FIELD_angle,
        FIELD_creationTime,
        FIELD_position,
        FIELD_weight,
        FIELD_CBT,
        FIELD_neighCBT,
        FIELD_power,
        FIELD_deltaF,
        FIELD_neighCBTTimestamp,
    };
  public:
    CAMPacketDescriptor();
    virtual ~CAMPacketDescriptor();

    virtual bool doesSupport(omnetpp::cObject *obj) const override;
    virtual const char **getPropertyNames() const override;
    virtual const char *getProperty(const char *propertyName) const override;
    virtual int getFieldCount() const override;
    virtual const char *getFieldName(int field) const override;
    virtual int findField(const char *fieldName) const override;
    virtual unsigned int getFieldTypeFlags(int field) const override;
    virtual const char *getFieldTypeString(int field) const override;
    virtual const char **getFieldPropertyNames(int field) const override;
    virtual const char *getFieldProperty(int field, const char *propertyName) const override;
    virtual int getFieldArraySize(omnetpp::any_ptr object, int field) const override;
    virtual void setFieldArraySize(omnetpp::any_ptr object, int field, int size) const override;

    virtual const char *getFieldDynamicTypeString(omnetpp::any_ptr object, int field, int i) const override;
    virtual std::string getFieldValueAsString(omnetpp::any_ptr object, int field, int i) const override;
    virtual void setFieldValueAsString(omnetpp::any_ptr object, int field, int i, const char *value) const override;
    virtual omnetpp::cValue getFieldValue(omnetpp::any_ptr object, int field, int i) const override;
    virtual void setFieldValue(omnetpp::any_ptr object, int field, int i, const omnetpp::cValue& value) const override;

    virtual const char *getFieldStructName(int field) const override;
    virtual omnetpp::any_ptr getFieldStructValuePointer(omnetpp::any_ptr object, int field, int i) const override;
    virtual void setFieldStructValuePointer(omnetpp::any_ptr object, int field, int i, omnetpp::any_ptr ptr) const override;
};

Register_ClassDescriptor(CAMPacketDescriptor)

CAMPacketDescriptor::CAMPacketDescriptor() : omnetpp::cClassDescriptor(omnetpp::opp_typename(typeid(CAMPacket)), "inet::FieldsChunk")
{
    propertyNames = nullptr;
}

CAMPacketDescriptor::~CAMPacketDescriptor()
{
    delete[] propertyNames;
}

bool CAMPacketDescriptor::doesSupport(omnetpp::cObject *obj) const
{
    return dynamic_cast<CAMPacket *>(obj)!=nullptr;
}

const char **CAMPacketDescriptor::getPropertyNames() const
{
    if (!propertyNames) {
        static const char *names[] = {  nullptr };
        omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
        const char **baseNames = base ? base->getPropertyNames() : nullptr;
        propertyNames = mergeLists(baseNames, names);
    }
    return propertyNames;
}

const char *CAMPacketDescriptor::getProperty(const char *propertyName) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    return base ? base->getProperty(propertyName) : nullptr;
}

int CAMPacketDescriptor::getFieldCount() const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    return base ? 13+base->getFieldCount() : 13;
}

unsigned int CAMPacketDescriptor::getFieldTypeFlags(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldTypeFlags(field);
        field -= base->getFieldCount();
    }
    static unsigned int fieldTypeFlags[] = {
        FD_ISCOMPOUND,    // FIELD_speed
        FD_ISEDITABLE,    // FIELD_beaconRate
        FD_ISEDITABLE,    // FIELD_source
        FD_ISEDITABLE,    // FIELD_sequenceNumber
        FD_ISEDITABLE,    // FIELD_angle
        FD_ISEDITABLE,    // FIELD_creationTime
        FD_ISCOMPOUND,    // FIELD_position
        FD_ISEDITABLE,    // FIELD_weight
        FD_ISEDITABLE,    // FIELD_CBT
        FD_ISEDITABLE,    // FIELD_neighCBT
        FD_ISEDITABLE,    // FIELD_power
        FD_ISEDITABLE,    // FIELD_deltaF
        FD_ISEDITABLE,    // FIELD_neighCBTTimestamp
    };
    return (field >= 0 && field < 13) ? fieldTypeFlags[field] : 0;
}

const char *CAMPacketDescriptor::getFieldName(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldName(field);
        field -= base->getFieldCount();
    }
    static const char *fieldNames[] = {
        "speed",
        "beaconRate",
        "source",
        "sequenceNumber",
        "angle",
        "creationTime",
        "position",
        "weight",
        "CBT",
        "neighCBT",
        "power",
        "deltaF",
        "neighCBTTimestamp",
    };
    return (field >= 0 && field < 13) ? fieldNames[field] : nullptr;
}

int CAMPacketDescriptor::findField(const char *fieldName) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    int baseIndex = base ? base->getFieldCount() : 0;
    if (strcmp(fieldName, "speed") == 0) return baseIndex + 0;
    if (strcmp(fieldName, "beaconRate") == 0) return baseIndex + 1;
    if (strcmp(fieldName, "source") == 0) return baseIndex + 2;
    if (strcmp(fieldName, "sequenceNumber") == 0) return baseIndex + 3;
    if (strcmp(fieldName, "angle") == 0) return baseIndex + 4;
    if (strcmp(fieldName, "creationTime") == 0) return baseIndex + 5;
    if (strcmp(fieldName, "position") == 0) return baseIndex + 6;
    if (strcmp(fieldName, "weight") == 0) return baseIndex + 7;
    if (strcmp(fieldName, "CBT") == 0) return baseIndex + 8;
    if (strcmp(fieldName, "neighCBT") == 0) return baseIndex + 9;
    if (strcmp(fieldName, "power") == 0) return baseIndex + 10;
    if (strcmp(fieldName, "deltaF") == 0) return baseIndex + 11;
    if (strcmp(fieldName, "neighCBTTimestamp") == 0) return baseIndex + 12;
    return base ? base->findField(fieldName) : -1;
}

const char *CAMPacketDescriptor::getFieldTypeString(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldTypeString(field);
        field -= base->getFieldCount();
    }
    static const char *fieldTypeStrings[] = {
        "inet::Coord",    // FIELD_speed
        "double",    // FIELD_beaconRate
        "int",    // FIELD_source
        "int",    // FIELD_sequenceNumber
        "double",    // FIELD_angle
        "omnetpp::simtime_t",    // FIELD_creationTime
        "inet::Coord",    // FIELD_position
        "double",    // FIELD_weight
        "double",    // FIELD_CBT
        "double",    // FIELD_neighCBT
        "double",    // FIELD_power
        "double",    // FIELD_deltaF
        "omnetpp::simtime_t",    // FIELD_neighCBTTimestamp
    };
    return (field >= 0 && field < 13) ? fieldTypeStrings[field] : nullptr;
}

const char **CAMPacketDescriptor::getFieldPropertyNames(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldPropertyNames(field);
        field -= base->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

const char *CAMPacketDescriptor::getFieldProperty(int field, const char *propertyName) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldProperty(field, propertyName);
        field -= base->getFieldCount();
    }
    switch (field) {
        default: return nullptr;
    }
}

int CAMPacketDescriptor::getFieldArraySize(omnetpp::any_ptr object, int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldArraySize(object, field);
        field -= base->getFieldCount();
    }
    CAMPacket *pp = omnetpp::fromAnyPtr<CAMPacket>(object); (void)pp;
    switch (field) {
        default: return 0;
    }
}

void CAMPacketDescriptor::setFieldArraySize(omnetpp::any_ptr object, int field, int size) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldArraySize(object, field, size);
            return;
        }
        field -= base->getFieldCount();
    }
    CAMPacket *pp = omnetpp::fromAnyPtr<CAMPacket>(object); (void)pp;
    switch (field) {
        default: throw omnetpp::cRuntimeError("Cannot set array size of field %d of class 'CAMPacket'", field);
    }
}

const char *CAMPacketDescriptor::getFieldDynamicTypeString(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldDynamicTypeString(object,field,i);
        field -= base->getFieldCount();
    }
    CAMPacket *pp = omnetpp::fromAnyPtr<CAMPacket>(object); (void)pp;
    switch (field) {
        default: return nullptr;
    }
}

std::string CAMPacketDescriptor::getFieldValueAsString(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldValueAsString(object,field,i);
        field -= base->getFieldCount();
    }
    CAMPacket *pp = omnetpp::fromAnyPtr<CAMPacket>(object); (void)pp;
    switch (field) {
        case FIELD_speed: return "";
        case FIELD_beaconRate: return double2string(pp->getBeaconRate());
        case FIELD_source: return long2string(pp->getSource());
        case FIELD_sequenceNumber: return long2string(pp->getSequenceNumber());
        case FIELD_angle: return double2string(pp->getAngle());
        case FIELD_creationTime: return simtime2string(pp->getCreationTime());
        case FIELD_position: return "";
        case FIELD_weight: return double2string(pp->getWeight());
        case FIELD_CBT: return double2string(pp->getCBT());
        case FIELD_neighCBT: return double2string(pp->getNeighCBT());
        case FIELD_power: return double2string(pp->getPower());
        case FIELD_deltaF: return double2string(pp->getDeltaF());
        case FIELD_neighCBTTimestamp: return simtime2string(pp->getNeighCBTTimestamp());
        default: return "";
    }
}

void CAMPacketDescriptor::setFieldValueAsString(omnetpp::any_ptr object, int field, int i, const char *value) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldValueAsString(object, field, i, value);
            return;
        }
        field -= base->getFieldCount();
    }
    CAMPacket *pp = omnetpp::fromAnyPtr<CAMPacket>(object); (void)pp;
    switch (field) {
        case FIELD_beaconRate: pp->setBeaconRate(string2double(value)); break;
        case FIELD_source: pp->setSource(string2long(value)); break;
        case FIELD_sequenceNumber: pp->setSequenceNumber(string2long(value)); break;
        case FIELD_angle: pp->setAngle(string2double(value)); break;
        case FIELD_creationTime: pp->setCreationTime(string2simtime(value)); break;
        case FIELD_weight: pp->setWeight(string2double(value)); break;
        case FIELD_CBT: pp->setCBT(string2double(value)); break;
        case FIELD_neighCBT: pp->setNeighCBT(string2double(value)); break;
        case FIELD_power: pp->setPower(string2double(value)); break;
        case FIELD_deltaF: pp->setDeltaF(string2double(value)); break;
        case FIELD_neighCBTTimestamp: pp->setNeighCBTTimestamp(string2simtime(value)); break;
        default: throw omnetpp::cRuntimeError("Cannot set field %d of class 'CAMPacket'", field);
    }
}

omnetpp::cValue CAMPacketDescriptor::getFieldValue(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldValue(object,field,i);
        field -= base->getFieldCount();
    }
    CAMPacket *pp = omnetpp::fromAnyPtr<CAMPacket>(object); (void)pp;
    switch (field) {
        case FIELD_speed: return omnetpp::toAnyPtr(&pp->getSpeed()); break;
        case FIELD_beaconRate: return pp->getBeaconRate();
        case FIELD_source: return pp->getSource();
        case FIELD_sequenceNumber: return pp->getSequenceNumber();
        case FIELD_angle: return pp->getAngle();
        case FIELD_creationTime: return pp->getCreationTime().dbl();
        case FIELD_position: return omnetpp::toAnyPtr(&pp->getPosition()); break;
        case FIELD_weight: return pp->getWeight();
        case FIELD_CBT: return pp->getCBT();
        case FIELD_neighCBT: return pp->getNeighCBT();
        case FIELD_power: return pp->getPower();
        case FIELD_deltaF: return pp->getDeltaF();
        case FIELD_neighCBTTimestamp: return pp->getNeighCBTTimestamp().dbl();
        default: throw omnetpp::cRuntimeError("Cannot return field %d of class 'CAMPacket' as cValue -- field index out of range?", field);
    }
}

void CAMPacketDescriptor::setFieldValue(omnetpp::any_ptr object, int field, int i, const omnetpp::cValue& value) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldValue(object, field, i, value);
            return;
        }
        field -= base->getFieldCount();
    }
    CAMPacket *pp = omnetpp::fromAnyPtr<CAMPacket>(object); (void)pp;
    switch (field) {
        case FIELD_beaconRate: pp->setBeaconRate(value.doubleValue()); break;
        case FIELD_source: pp->setSource(omnetpp::checked_int_cast<int>(value.intValue())); break;
        case FIELD_sequenceNumber: pp->setSequenceNumber(omnetpp::checked_int_cast<int>(value.intValue())); break;
        case FIELD_angle: pp->setAngle(value.doubleValue()); break;
        case FIELD_creationTime: pp->setCreationTime(value.doubleValue()); break;
        case FIELD_weight: pp->setWeight(value.doubleValue()); break;
        case FIELD_CBT: pp->setCBT(value.doubleValue()); break;
        case FIELD_neighCBT: pp->setNeighCBT(value.doubleValue()); break;
        case FIELD_power: pp->setPower(value.doubleValue()); break;
        case FIELD_deltaF: pp->setDeltaF(value.doubleValue()); break;
        case FIELD_neighCBTTimestamp: pp->setNeighCBTTimestamp(value.doubleValue()); break;
        default: throw omnetpp::cRuntimeError("Cannot set field %d of class 'CAMPacket'", field);
    }
}

const char *CAMPacketDescriptor::getFieldStructName(int field) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldStructName(field);
        field -= base->getFieldCount();
    }
    switch (field) {
        case FIELD_speed: return omnetpp::opp_typename(typeid(inet::Coord));
        case FIELD_position: return omnetpp::opp_typename(typeid(inet::Coord));
        default: return nullptr;
    };
}

omnetpp::any_ptr CAMPacketDescriptor::getFieldStructValuePointer(omnetpp::any_ptr object, int field, int i) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount())
            return base->getFieldStructValuePointer(object, field, i);
        field -= base->getFieldCount();
    }
    CAMPacket *pp = omnetpp::fromAnyPtr<CAMPacket>(object); (void)pp;
    switch (field) {
        case FIELD_speed: return omnetpp::toAnyPtr(&pp->getSpeed()); break;
        case FIELD_position: return omnetpp::toAnyPtr(&pp->getPosition()); break;
        default: return omnetpp::any_ptr(nullptr);
    }
}

void CAMPacketDescriptor::setFieldStructValuePointer(omnetpp::any_ptr object, int field, int i, omnetpp::any_ptr ptr) const
{
    omnetpp::cClassDescriptor *base = getBaseClassDescriptor();
    if (base) {
        if (field < base->getFieldCount()){
            base->setFieldStructValuePointer(object, field, i, ptr);
            return;
        }
        field -= base->getFieldCount();
    }
    CAMPacket *pp = omnetpp::fromAnyPtr<CAMPacket>(object); (void)pp;
    switch (field) {
        default: throw omnetpp::cRuntimeError("Cannot set field %d of class 'CAMPacket'", field);
    }
}

namespace omnetpp {

}  // namespace omnetpp

