//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET4_3_CAMGENERATOR_H_
#define __INET4_3_CAMGENERATOR_H_

#include <omnetpp.h>
#include <string>
#include "inet/mobility/contract/IMobility.h"
#include "inet/physicallayer/wireless/common/radio/packetlevel/Radio.h"
#include "inet/physicallayer/wireless/common/contract/packetlevel/IRadio.h"
#include "inet/common/packet/Packet.h"
#include "VehicleTable.h"
#define CAM_GENERATION_TO 0
#define CAM_PKT 1

using namespace omnetpp;

namespace inet {

class MaxDif: public cObject {
public:
    MaxDif(int window=0);
    void reset() ;
    bool addSample(double s);

    int windowSize;
    int samples;
    double max;
    double min;
    std::string  toString() const;
};

std::ostream& operator<<(std::ostream& out, const MaxDif& obj);

struct PDR{
      unsigned int vehicles, received;
  };
class CAMReceivedInfo: public cObject, noncopyable {
public:
    int id;
    int source;
    int sequenceNumber;
    Coord position;

};
class CAMGenerator :public cSimpleModule, public cListener
{

public:
    CAMGenerator();
    virtual ~CAMGenerator();

    static simsignal_t changeMinBeaconRateSignal;
    static simsignal_t changeMinPowerSignal;
    static simsignal_t cbtSignal;
    virtual void receiveSignal(cComponent *source, simsignal_t signal, intval_t value, cObject *details) override;
    virtual void receiveSignal(cComponent *source, simsignal_t signalID, cObject *obj, cObject *details) override;
protected:
    static simsignal_t beaconRateSignal;
    static simsignal_t diff_maxSignal;
    static simsignal_t sqr_mRateSignal;
    static simsignal_t camReceivedSignal;
    virtual void initialize(int stage) override;
    virtual int numInitStages() const override {return NUM_INIT_STAGES;}
    virtual void handleMessage(cMessage *msg) override;
    virtual void sendDown(Packet* p);
    virtual void sendCAM();
    virtual void setBeaconRate(double br);
    virtual void handleCAM(cMessage* msg);
    virtual void rescheduleBeacon(double rate);
    virtual void computePDR();
    virtual const simtime_t& getCbtWindow() const;
    virtual void setCbtWindow(const simtime_t& cbtWindow, double offset=-1);
    virtual double getMeasuredCBT(double period);

    double beaconRate;
    double maxBeaconRate;
    double minBeaconRate;
    double beaconTime;
    double channelBitRate;
    bool disableGeneration;
    int beaconSize;
    int lowerLayerIn;
    int lowerLayerOut;
    int diffWindow;
    int CAMreceived;
    int CAMsent;
    int numberOfNodes;
    int sentCAMReceivedSignals;
    MaxDif mdiff;


    std::vector<double> pdrRanges;
    std::vector<PDR> pdrAtDistance;
    std::vector<simsignal_t> pdrSignals;
    IMobility* mob;
    physicallayer::Radio* radio;
    int myId;
    cMessage* beaconGenerationTimer;
    simtime_t initTime;
    VehicleTable* vehicleTable;
    double jitter;
    VehicleInfo* info;

    //PDR
    const physicallayer::ITransmission* currentTransmission;
    int currentTransmissionSeqNumber;

    //CBT measurement

    simtime_t cbtWindow;
    simtime_t cbt_rtime;
    simtime_t lu_rtime;
    simtime_t cbt_txtime;
    simtime_t lu_txtime;
    simtime_t cbt_idletime;
    simtime_t lu_idletime;
    cMessage* cbfSampleTimer;
    physicallayer::IRadio::ReceptionState lastReceptionState;
    physicallayer::IRadio::TransmissionState lastTransmissionState;


};

} //namespace

#endif
