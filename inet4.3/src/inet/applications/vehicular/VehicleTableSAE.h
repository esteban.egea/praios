//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET4_3_VEHICLETABLESAE_H_
#define __INET4_3_VEHICLETABLESAE_H_

#include <omnetpp.h>
#include "VehicleTable.h"
#include "VehicleInfo.h"
using namespace omnetpp;

namespace inet {
typedef std::map<int,std::vector<VehicleInfo*> > VTableSAE;
/**
 * TODO - Generated class
 */
class VehicleTableSAE : public VehicleTable
{
public:
    virtual ~VehicleTableSAE();
    virtual int insertOrUpdate(VehicleInfo* info) override;
    virtual void refreshTableVPERInterval();
    virtual void refreshTableVPERSubInterval();
    virtual unsigned int computeVehicleDensityInRange();
    virtual double computeChannelQualityIndicator();
    virtual double getCQI() const;
    virtual double getVDR() const;

  protected:

    virtual void initialize() override;
    virtual void handleMessage(cMessage *msg) override;
    VTableSAE vtsae;
    unsigned int vPERInterval;
    double vPERSubInterval;
    double vPERRange ;
    double rsquared;
    double vPERMax;
    cMessage* vPERIntervalTimer;
    unsigned int currentSubInterval;
    double currentCQI;
    unsigned int currentVDR;
};

} //namespace

#endif
