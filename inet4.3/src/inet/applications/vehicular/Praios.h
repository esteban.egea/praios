//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET4_3_RAYLEIGHPOWERRATE_H_
#define __INET4_3_RAYLEIGHPOWERRATE_H_

#include <omnetpp.h>
#include <vector>
#include "CAMGenerator.h"

#include "inet/physicallayer/wireless/common/contract/packetlevel/IRadio.h"

using namespace omnetpp;

namespace inet {

/**
 * TODO - Generated class
 */
class Praios : public CAMGenerator
{
public:
    Praios();
    virtual void receiveSignal(cComponent *source, simsignal_t signalID, double d);

    virtual ~Praios();
protected:
    int MBL_bps;
    double MBL_cbt;
    double h;
    double y;
    double ymin;
    double ymax;
    double hmin;
    double hmax;
    double currentPower;
    double SAm;
    double Ko;
    double hpathloss;
    double C; //MBL in beacons/s as used in the paper
    double Cfraction; //MBL as a fraction of channel use (cbt)
    double channelCapacityMS; //Maximum number of messages/s the channel can send
    double camtime; // duration of a CAM packet (including PHY and MAC headers and preamble)
    bool useCBT;
    bool disableControl;
    double weight;
    double alpha;
    double receivedRate;
    double subGradient;
    double minWeight;
    double priority;
    bool synchronous;
    double syncPeriod;
    bool useJitter;
    bool useLocalGradMod;

    bool useSplit;

    std::vector<double> beconRateRange;
    std::vector<double> powerRange;
    double epsilon;
    double betaStep; //Gradient step for dual ascent (lambdas)
    double gammaStep; //Gradient step for local gradient

    static simsignal_t powerSignal;
    static simsignal_t totalReceivedRate;
    static simsignal_t emitedRate;
    static simsignal_t subgradientSignal;
    static simsignal_t totalWeightSignal;
    static simsignal_t myWeightSignal;

   double lastCBT;

    cMessage* updateRateTimer;
    cMessage* updateWeightTimer;





    virtual void initialize(int stage) override;
    virtual void handleMessage(cMessage *msg) override;

    virtual void sendCAMAtPower(double br, double p);
    virtual void computeSubgradient();
    virtual void updateWeight();
    virtual void updateRateAndPower();
    virtual void localGradientProjection();
    virtual void localGradientProjectionMod();
    virtual void localGradientProjectionRegularized();


};

} //namespace

#endif
