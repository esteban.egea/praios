//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "FileNodeManager.h"
#include <fstream>
#include <sstream>
#include <iostream>

namespace inet {

Define_Module(FileNodeManager);

void FileNodeManager::initialize(int stage)
{
    //Cannot be called in INITSTAGE_LOCAL because with callInitialize it will call all the stages of the submodules and
    //modules outside of this may have not been created and initialized yet, e.g. RadioMedium.
    //Let us wait at the end...
    if (stage==INITSTAGE_LAST) {
        EV<<"Initializing FileNodeManager"<<endl;

        moduleType = par("moduleType").stdstringValue();
        moduleName = par("moduleName").stdstringValue();
        moduleDisplayString = par("moduleDisplayString").stdstringValue();
        const char * filename=par("filename");

        dim=par("dim");
        nodeVectorIndex=0;
        maxX=par("maxX");
        maxY=par("maxY");
        destination=par("destination");
        std::ifstream in(filename, std::ios::in);

        if (in.fail()) {
            throw cRuntimeError("Cannot open file '%s'", filename);
        }

        std::string line;
        cModule* parentmod = getParentModule();
        if (!parentmod) error("Parent Module not found");

        while (std::getline(in, line))
        {
            double xc;
            double yc;
            std::string delimiters("\t");
            std::istringstream iline;
            std::string val;
            std::cout<<line<<std::endl;
            iline.str(line);
            getline(iline,val,'\t');
            xc=std::stof(val);
            getline(iline,val,'\t');
            yc=std::stof(val);
            EV<<"Position of node "<<nodeVectorIndex<<" ("<<xc<<","<<yc<<")"<<endl;



            cModuleType* nodeType = cModuleType::get(moduleType.c_str());
            if (!nodeType) error("Module Type \"%s\" not found", moduleType.c_str());

            //No vector of submodules
            //std::string mn=moduleName+ "-"+std::to_string(nodeVectorIndex);
            //cModule* mod = nodeType->create(mn.c_str(), parentmod);

//With Omnet 6
#if OMNETPP_BUILDNUM >= 1525
            //cModule* mod = nodeType->create(moduleName.c_str(), parentmod, nodeVectorIndex);
            parentmod->setSubmoduleVectorSize("node", nodeVectorIndex + 1);
#endif
            cModule* mod = nodeType->create(moduleName.c_str(), parentmod, nodeVectorIndex);
            mod->finalizeParameters();
            mod->getDisplayString().parse(moduleDisplayString.c_str());
            mod->buildInside();
            cPar& id=mod->par("id");
            id.setIntValue(nodeVectorIndex);

            cPar& y = mod->getSubmodule("mobility")->par("initialY");
            y.setDoubleValue(yc);
            mod->recordScalar("initialY",yc);
            cPar& x = mod->getSubmodule("mobility")->par("initialX");
            x.setDoubleValue(xc);
            mod->recordScalar("initialX",xc);
            mod->scheduleStart(simTime());
            mod->callInitialize();
            // std::cout<<dist<<"\t0.0"<<endl;


            nodeVectorIndex++;


        }
        in.close();
    }

}





} //namespace
