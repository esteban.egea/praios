//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "TRCNodeManager.h"

namespace inet {

Define_Module(TRCNodeManager);

void TRCNodeManager::initialize(int stage)
{

    if (stage==INITSTAGE_LOCAL) {
        EV<<"Initializing TRCNodeManager"<<endl;
        moduleType = par("moduleType").stdstringValue();
        moduleName = par("moduleName").stdstringValue();
        moduleDisplayString = par("moduleDisplayString").stdstringValue();

        dim=par("dim");
        nodeVectorIndex=0;
        maxX=par("maxX");
        maxY=par("maxY");
        destination=par("destination");
        cModule* source=NULL;
        if (dim==1) {
            double dist=0.0;
            while(dist<=maxX) {
                double s=par("separation");
                cModule* parentmod = getParentModule();
                if (!parentmod) error("Parent Module not found");

                cModuleType* nodeType = cModuleType::get(moduleType.c_str());
                if (!nodeType) error("Module Type \"%s\" not found", moduleType.c_str());


                cModule* mod = nodeType->create(moduleName.c_str(), parentmod, nodeVectorIndex);
                mod->finalizeParameters();
                mod->getDisplayString().parse(moduleDisplayString.c_str());
                mod->buildInside();
                //mod->scheduleStart(simTime() );
                //mod->callInitialize();
                cPar& x = mod->getSubmodule("mobility")->par("initialX");
                x.setDoubleValue(dist);
                // std::cout<<dist<<"\t0.0"<<endl;
                mod->recordScalar("initialX",dist);
                mod->recordScalar("initialY",0.0);

                /* cPar& y = mod->getSubmodule("mobility")->par("initialY");
                             y.setDoubleValue(0.0);
                             cPar& z = mod->getSubmodule("mobility")->par("initialZ");
                                                z.setDoubleValue(0.0);
                 */
                dist+=s;
                if (nodeVectorIndex==0) {
                    source=mod;
                }
                nodeVectorIndex++;

            }

            if (destination) {
                //Destination module
                cModule* parentmod = getParentModule();
                if (!parentmod) error("Parent Module not found");

                cModuleType* nodeType = cModuleType::get(moduleType.c_str());
                if (!nodeType) error("Module Type \"%s\" not found", moduleType.c_str());


                cModule* mod = nodeType->create(moduleName.c_str(), parentmod, nodeVectorIndex);
                mod->finalizeParameters();
                mod->getDisplayString().parse(moduleDisplayString.c_str());
                mod->buildInside();
                //mod->scheduleStart(simTime() );
                //mod->callInitialize();
                cPar& x = mod->getSubmodule("mobility")->par("initialX");
                x.setDoubleValue(maxX-1);
                cPar& dest=mod->getSubmodule("router")->par("destination");
                dest.setIntValue(nodeVectorIndex);

                /* cPar& y = mod->getSubmodule("mobility")->par("initialY");
                                                        y.setDoubleValue(0.0);
                                                        cPar& z = mod->getSubmodule("mobility")->par("initialZ");
                                                                           z.setDoubleValue(0.0);
                 */
                cPar& dest2=source->getSubmodule("router")->par("destination");
                dest2.setIntValue(nodeVectorIndex);
                cPar& destPos=source->getSubmodule("router")->par("destinationPosition");
                destPos.setDoubleValue(maxX-0.2);
            }
        }

    }

}
int TRCNodeManager::getNumberOfNodes() {
    return (nodeVectorIndex +1);
}




} //namespace
