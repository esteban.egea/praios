//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef __INET4_3_BFPC_H_
#define __INET4_3_BFPC_H_

#include <omnetpp.h>
#include "CAMGenerator.h"

#include "inet/physicallayer/wireless/common/contract/packetlevel/IRadio.h"
using namespace omnetpp;

namespace inet {

/**
 * TODO - Generated class
 */
class BFPC : public CAMGenerator
{
protected:
    virtual void initialize(int stage) override;
    virtual void handleMessage(cMessage *msg) override;
    virtual void sendCAMAtPower(double br, double p);
    virtual void performControlActions();
    std::vector<double> beconRateRange;
    std::vector<double> powerRange;

    static simsignal_t powerSignal;
    bool synchronous;
    double u;
    double w;
    double c;
    double controlPeriod;
    double currentPower;
    double maxPower;
    double minPower;
    double maxRate;
    double minRate;
    double camtime; // duration of a CAM packet (including PHY and MAC headers and preamble)

    cMessage* controlTimer;
};

} //namespace

#endif
